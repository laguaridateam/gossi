<?php

// BEGIN iThemes Security - No modifiques ni borres esta línea
// iThemes Security Config Details: 2
define( 'FORCE_SSL_ADMIN', true ); // Redirigir todas las solicitud de páginas HTTP a HTTPS - Seguridad > Ajustes > Forzar SSL
define( 'DISALLOW_FILE_EDIT', true ); // Desactivar editor de archivos - Seguridad > Ajustes > Ajustes WordPress > Editor de archivos
// END iThemes Security - No modifiques ni borres esta línea

define( 'ITSEC_ENCRYPTION_KEY', 'XTxZZmUxbTViSmBKW3NAeXxla2VPZFJyeWU/ZUhSbWFUcmM9UiZDfiZZWjhfLGhJUHIjQihaTGRleCR0R2AqRA==' );

/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache


define('WP_MEMORY_LIMIT', '512M');


// ** MySQL settings - You can get this info from your web host ** //
define( 'DB_NAME', "gossicom_k1w1n3l0n" );
define( 'DB_USER', "gossicom_c3r3z4m0r4" );
define( 'DB_PASSWORD', "PL*+T[JkFVaU" );
define( 'DB_HOST', "localhost" );
define( 'DB_CHARSET', 'utf8mb4' );
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'wIV^g#.4wv~fy}ktkN`~lR9,,K({TG:uMWCKT$g}q5l#Pf[%J{,{K,}6FyhwK~6]' );
define( 'SECURE_AUTH_KEY',  'qg#ME6uPW5PESqi#[Btw^-V/trebM/Tn%?:cuf3a_otWbHoxPhFf8Gxr.3r%nXF]' );
define( 'LOGGED_IN_KEY',    'OzZrV%vdu*i+m7MmPJ*g^i3Xjh;GYIQ~aVK,=2Z//_2olB`?S)saL7#BdFr/)U0&' );
define( 'NONCE_KEY',        'O?W(T_.OO4)nS%,IEy|:Zz?>9J;aiJT.~bXEu?K9ZbGC.l4=(H7>yc5j7@FU=AQp' );
define( 'AUTH_SALT',        'p8l~=ROI<wgdlQonRedg{S)-F^}!r $/os65(k4#,Hm1c.la*?<ed:|YHks[AH@J' );
define( 'SECURE_AUTH_SALT', '8Gl]@ciNkm->deS*&^;slqQSAAR+dH)?_nF6A.1PXq .72tgE$q{Q8GHb8S@|kFz' );
define( 'LOGGED_IN_SALT',   'CXgO3CGb[Rk^z}*9v,v?)NgN? h fIhzbTaNL4eXz`.<Jos@|Zc,k}Fp&&Ks0!QA' );
define( 'NONCE_SALT',       'N:5$aINRsalS3DD7[eSY-Q)]rXD5U2:<8dn@EMFUNRc/(B]i|uIZcnMg!GJJ.}0`' );
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );


/* That's all, stop editing! Happy publishing. */
/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
