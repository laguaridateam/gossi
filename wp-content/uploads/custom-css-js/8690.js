<!-- start Simple Custom CSS and JS -->
<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAeTdzvvQ2nQLaBfTnj5Hc_VVgZXohgrs0'></script>

Si estás usando la biblioteca jQuery, entonces no olvides envolver tu código dentro de jQuery.ready() así:

jQuery(document).ready(function( $ ){
    // Tu código aquí dentro
});

--

Si quieres enlazar a un archivo JavaScript que resida en otro servidor (como
<script src="https://example.com/your-js-file.js"></script>), entonces, por favor, usa
la página «Añadir código HTML» , ya que es un código HTML que enlaza a un archivo JavaScript.

Fin del comentario */ 

<!-- end Simple Custom CSS and JS -->
