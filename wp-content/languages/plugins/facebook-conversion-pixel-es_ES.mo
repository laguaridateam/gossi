��    n      �  �   �      P	     Q	  
   S	     ^	     e	     l	     u	  	   �	  	   �	     �	     �	     �	     �	  	   �	  	   �	     �	     �	     �	     
  
   
     
     3
     C
     I
     Y
     h
     y
     �
     �
     �
     �
     �
     �
  
   �
  ;   �
        
                  &     @     \     n     z     �  
   �     �     �     �  2   �  :   �  `        {  
   �     �  9   �     �     �  1   �       
        %     *     .  	   4  
   >  .   I     x  	   �     �     �     �     �     �     �     �     �                     %     .     5     K     X     f     o          �     �     �     �     �  !   �  >   �     9     A  
   F     Q     W     _     g     z     �     �  	   �     �     �     �     �  ?  �     #     %  	   1  	   ;     E     M     k     y     �     �     �     �     �     �     �     �                 )   (     R     j     q     �     �     �     �     �     �  	   �     �  	   �       T        i     p     �     �  !   �  "   �     �     �     �               #     +     0  H   A  c   �  c   �     R     b  	   n  L   x     �  	   �  L   �     #     4     @     H     N  
   W     b  =   n      �  	   �     �     �               &     E     N     `     o          �     �     �      �     �     �  
                   @     G     X     j     q     �  ?   �     �     �     �       	     
             ,     >     D     P     `     t     }     �     Z   F                      K      d           n                  S   H   2   )       '       E   1   T           3   !   $   @          ]      <              k              X   0   U   `       _   m       6   ,       %          5   +         W             C   J   h   l       A   /              R   Y      :          f   \   L   Q       D       #                 7   M   V   g   P          *   I   "   j       b   ?   i                >      (         ;   O      [          
   ^      8   &   G   a   c   =       4   	       e   9      N   -       B       .        % (Pro Only) (hide) (show) Activate Active - Click to Pause Add Group Add Image Add New Add Row Add or Upload File Add or Upload Files All Pages Blog Page Cancel Change Choose Time Clear Click here Click here for instructions Click to toggle Close Configure Pixel Confirm Delete Conversion Pixel Current Color Custom Event Default Delete Dismiss Done Download Edit Event Enable Facebook Conversion Pixel field on these post types: Event Event Name Events Exclude Users Facebook Conversion Pixel Facebook Conversion Pixel:  Facebook Pixel ID Fatcat Apps Feed URL File: Front Page Help:  Hour Insert Code Insert Facebook Conversion Pixel code on this page Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec January, February, March, April, May, June, July, August, September, October, November, December Learn More... Loading... Main Metabox configuration is required to have an ID parameter Minute Next No oEmbed Results Found for %s. View more info at No terms No, thanks None Now Pages Parameter Parameters Paste your Facebook Conversion Pixel code here Paused - Click to Activate Pixel Cat Pixel Cat:  Please Try Again Plugin Usage Prev Remind me next week Remove Remove Embed Remove Group Remove Image Run Better Ads >> Save Scroll % Second Select / Deselect All Select Color Send Feedback Settings Settings saved. Setup Instructions Skip Skip This Step Standard Events Status Su, Mo, Tu, We, Th, Fr, Sa Sun, Mon, Tue, Wed, Thu, Fri, Sat Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday Support Time Time delay Today Trigger Upgrade Upgrade to Premium Use this file Value WooCommerce Your Info Your name and email. hh:mm TT https://fatcatapps.com/ mm/dd/yy PO-Revision-Date: 2020-04-14 19:18:37+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: es
Project-Id-Version: Plugins - Pixel Cat &#8211; Conversion Pixel Manager - Development (trunk)
 % (sólo Pro) (ocultar) (mostrar) Activar Activo - haz clic para pausar Agregar grupo Añadir Imagen Añadir nuevo Añadir fila Añadir o Cargar archivo Añadir o subir archivos Todas las páginas Página Blog Cancelar Cambiar Elegir hora Borrar Haz clic aquí Haz clic aquí para obtener instrucciones Haga clic para alternar Cerrar Configurar píxel Confirmar eliminación Píxel de conversión Seleccionar color Evento personalizado Por defecto Eliminar Descartar Hecho Descargar Editar evento Activa el campo para el píxel de conversión de Facebook en este tipo de contenido: Evento Nombre del evento Eventos Excluir usuarios Píxel de conversión de Facebook Píxel de conversión de Facebook: Facebook Pixel ID Apps Fatcat URL del Feed Archivo: Página inicio Ayuda:  Hora Insertar código Inserta el código del píxel de conversión de Facebook en esta página Enero, febrero, marzo, abril, mayo, junio, julio, agosto, septiembre, octubre, noviembre, diciembre Enero, febrero, marzo, abril, mayo, junio, julio, agosto, septiembre, Octubre, Noviembre, Diciembre Aprender más.. Cargando... Principal Se requiere de configuración Metabox tener un parámetro de identificación Minuto Siguiente No se han encontrado resultados de oEmbed para %s. Ver más información en  No hay términos No, gracias Ninguno Ahora Páginas Parámetro Parámetros Pega aquí el código de tu píxel de conversión de Facebook En pausa - Haz clic para activar Pixel Cat Categoría del píxel:  Por favor, inténtalo de nuevo Uso del plugin Anterior Recuérdame la próxima semana Eliminar Eliminar Insertar Eliminar grupo Eliminar imagen Ejecuta mejores anuncios >> Guardar % desplazamiento Segundo Seleccionar / Deseleccionar todo Seleccionar color Enviar comentarios Configurar Ajustes guardados. Instrucciones de configuración Saltar Omitir este paso Eventos estándar Estado Do, Lu, Ma, Mi, Ju, Vi, Sa Sun, L, M, X, J, V, S El domingo, lunes, martes, miércoles, jueves, viernes, sábado Soporte Hora Retardo del tiempo Hoy Activador Actualizar Mejora a Premium Usar este archivo Valor WooCommerce Tu información Tu nombre y correo. hh:mm TT https://fatcatapps.com/ mm / dd / aa 