<?php
/**
 * Goya functions and definitions.
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Goya Child
 */
function goya_child_enqueue_styles() {

    wp_enqueue_style( 'goya-style' , get_template_directory_uri() . '/style.css' );
    
    wp_enqueue_style( 'goya-child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( 'goya-style' ),
        wp_get_theme()->get('Version')
    );
}

add_action(  'wp_enqueue_scripts', 'goya_child_enqueue_styles' );

add_filter('use_block_editor_for_post', '__return_false', 10);

// Show banner after product listings
add_action( 'woocommerce_after_main_content', 'custom_product_banner');
function custom_product_banner() {
    $output = '
<div class="row" style="background-color:#B9A16A;">
    <div style="text-align:center;margin:0 auto;">
        <h5 style="padding:2rem;color:white;margin:0;">Los cambios podrán realizarse hasta 15 días una vez realizada la compra. La prenda deberá estar en perfecta estado y sin uso. <br><a href="https://gossi.com.ar/politicas-de-cambios-devoluciones/" style="color:white"><u>Política de Cambios y Devoluciones</u></a></h5>
    </div>
</div>
    ';
    echo $output;
}

// Show title woocommerce-shop-look

add_action('woocommerce_single_product_summary', 'shop_the_look_title', 100);
function shop_the_look_title() {
    global $product;
    $id = $product->get_id();
    $shopTheLookProductIds = get_post_meta($product->get_id(), 'woocommerce_shop_look_products', true);
	if(empty($shopTheLookProductIds)) {
		return;
	} else {
        $output = '
<div>
    <h3 style="margin-top: 1rem;margin-bottom: 0.5rem;">
    COMBINÁ TU OUTFIT CON:</h3>
</div>';
        echo $output;
	    
	}    
}


// Agregar un elemento debajo del precio en el single_product_summary
add_filter( 'woocommerce_get_price_html', 'change_displayed_sale_price_html', 10, 2 );
function change_displayed_sale_price_html( $price, $product ) {
    
    
    
         $sale_price = (float) $product->get_price(); // Active price (the "Sale price" when on-sale)

     
         $precision = 2; // Max number of decimals
        
            $cuotapreciosale = round ($sale_price / 6, $precision);
            $price .= sprintf( __('<br><span style="font-size:15px;color:#282828;"> <strong>6</strong>&nbsp;CUOTAS SIN INTERES DE $%s</span>', 'woocommerce' ), $cuotapreciosale );
         
        
    return $price;
    
    
}

// Change field type to number woocommerce checkout
 
function bbloomer_change_checkout_field_input_type() {
echo "<script>document.getElementById('billing_postcode').type = 'number';</script>";
}
  
add_action( 'woocommerce_after_checkout_form', 'bbloomer_change_checkout_field_input_type');



/**
 * Hide shipping rates when free shipping is available, but keep "Local pickup" 
 * Updated to support WooCommerce 2.6 Shipping Zones
 */

function hide_shipping_when_free_is_available( $rates, $package ) {
	$new_rates = array();
	foreach ( $rates as $rate_id => $rate ) {
		// Only modify rates if free_shipping is present.
		if ( 'free_shipping' === $rate->method_id ) {
			$new_rates[ $rate_id ] = $rate;
			break;
		}
	}

	if ( ! empty( $new_rates ) ) {
		//Save local pickup if it's present.
		foreach ( $rates as $rate_id => $rate ) {
			if ('local_pickup' === $rate->method_id ) {
				$new_rates[ $rate_id ] = $rate;
				break;
			}
		}
		return $new_rates;
	}

	return $rates;
}

add_filter( 'woocommerce_package_rates', 'hide_shipping_when_free_is_available', 10, 2 );


