#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Goya Child Theme\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-04 03:17+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.5.1; wp-5.6.2\n"
"X-Domain: goya"

#. Author of the theme
msgid "Everthemes"
msgstr ""

#. Name of the theme
msgid "Goya Child Theme"
msgstr ""

#. Author URI of the theme
msgid "http://themeforest.net/user/luisvelaz"
msgstr ""

#. Description of the theme
msgid "This is a child theme of Goya."
msgstr ""
