msgid ""
msgstr ""
"Content-Type: text/plain; charset=utf-8\n"
"X-Generator: babel-plugin-makepot\n"

#: backend.js
msgid "Pricing Rules"
msgstr ""

#: backend.js
msgid "Add New Rule"
msgstr ""

#: backend.js
msgid "Back"
msgstr ""

#: backend.js
msgid "Product Lists"
msgstr ""

#: backend.js
msgid "Add New List"
msgstr ""

#: backend.js
msgid "Product Settings"
msgstr ""

#: backend.js
msgid "IN"
msgstr ""

#: backend.js
msgid "NOT IN"
msgstr ""

#: backend.js
msgid "Select Relation"
msgstr ""

#: backend.js
msgid "No attributes found"
msgstr ""

#: backend.js
msgid "Select Attribute"
msgstr ""

#: backend.js
msgid "Type to Select Attribute Type"
msgstr ""

#: backend.js
msgid "Attributes"
msgstr ""

#: backend.js
msgid "AND"
msgstr ""

#: backend.js
msgid "OR"
msgstr ""

#: backend.js
msgid "Add Relations"
msgstr ""

#: backend.js
msgid "*Please set a value greater than 0"
msgstr ""

#: backend.js
msgid "*Please set a value for Buy Unit Quantity"
msgstr ""

#: backend.js
msgid "*Discount Items Count value should be less than "
msgstr ""

#: backend.js
msgid "Percentage"
msgstr ""

#: backend.js
msgid "Fixed"
msgstr ""

#: backend.js
msgid "Free"
msgstr ""

#: backend.js
msgid "Buy Unit Quantity"
msgstr ""

#: backend.js
msgid "Discount Items Count"
msgstr ""

#: backend.js
msgid "Discount Type"
msgstr ""

#: backend.js
msgid "Select one"
msgstr ""

#: backend.js
msgid "Discount Value"
msgstr ""

#: backend.js
msgid "Value"
msgstr ""

#: backend.js
msgid "Remove"
msgstr ""

#: backend.js
msgid "Add"
msgstr ""

#: backend.js
msgid "Any Product"
msgstr ""

#: backend.js
msgid "Product / Product List"
msgstr ""

#: backend.js
msgid "Type the first letter to start searching"
msgstr ""

#: backend.js
msgid "Search Product / Product List"
msgstr ""

#: backend.js
msgid "Offer Products"
msgstr ""

#: backend.js
msgid "All"
msgstr ""

#: backend.js
msgid "Equal to"
msgstr ""

#: backend.js
msgid "Less than"
msgstr ""

#: backend.js
msgid "Less than or Equal"
msgstr ""

#: backend.js
msgid "Greater than"
msgstr ""

#: backend.js
msgid "Greater than or Equal"
msgstr ""

#: backend.js
msgid "Product price"
msgstr ""

#: backend.js
msgid "Product in cart"
msgstr ""

#: backend.js
msgid "Equals"
msgstr ""

#: backend.js
msgid "Not equals"
msgstr ""

#: backend.js
msgid "User selection"
msgstr ""

#: backend.js
msgid "User role"
msgstr ""

#: backend.js
msgid "Number of orders"
msgstr ""

#: backend.js
msgid "Amount spent"
msgstr ""

#: backend.js
msgid "Last order amount (Completed Orders)"
msgstr ""

#: backend.js
msgid "Previous order (Completed / Processing Orders)"
msgstr ""

#: backend.js
msgid "Contains"
msgstr ""

#: backend.js
msgid "Not contains"
msgstr ""

#: backend.js
msgid "Cart total amount (Product list)"
msgstr ""

#: backend.js
msgid "Cart total amount"
msgstr ""

#: backend.js
msgid "Cart total quantity (Product list)"
msgstr ""

#: backend.js
msgid "Cart total quantity"
msgstr ""

#: backend.js
msgid "Cart items count"
msgstr ""

#: backend.js
msgid "Payment method"
msgstr ""

#: backend.js
msgid "Not equal to"
msgstr ""

#: backend.js
msgid "Shipment method"
msgstr ""

#: backend.js
msgid "Select User Role"
msgstr ""

#: backend.js
msgid "Select User"
msgstr ""

#: backend.js
msgid "Select Products"
msgstr ""

#: backend.js
msgid "Select Discount Mode"
msgstr ""

#: backend.js
msgid "Remove Group"
msgstr ""

#: backend.js
msgid "Add Group"
msgstr ""

#: backend.js
msgid "Any product"
msgstr ""

#: backend.js
msgid "Condition"
msgstr ""

#: backend.js
msgid "Select One"
msgstr ""

#: backend.js
msgid "Choose a Product"
msgstr ""

#: backend.js
msgid "Type to Select a Product"
msgstr ""

#: backend.js
msgid "No products found"
msgstr ""

#: backend.js
msgid "Type to Select Variations"
msgstr ""

#: backend.js
msgid "No variations found"
msgstr ""

#: backend.js
msgid "Free Products"
msgstr ""

#: backend.js
msgid "Search Product"
msgstr ""

#: backend.js
msgid "Category"
msgstr ""

#: backend.js
msgid "Search Category"
msgstr ""

#: backend.js
msgid "Amount Spent (Greater Than)"
msgstr ""

#: backend.js
msgid "Amount"
msgstr ""

#: backend.js
msgid "User Role"
msgstr ""

#: backend.js
msgid "Fixed price"
msgstr ""

#: backend.js
msgid "Minimum Quantity"
msgstr ""

#: backend.js
msgid "Maximum Quantity"
msgstr ""

#: backend.js
msgid "Refers to the discount value / percentage, not the price of the product."
msgstr ""

#: backend.js
msgid "From Date / Time"
msgstr ""

#: backend.js
msgid "To Date / Time"
msgstr ""

#: backend.js
msgid "End date should be greater than start date."
msgstr ""

#: backend.js
msgid "Pick a date."
msgstr ""

#: backend.js
msgid "Select Variations"
msgstr ""

#: backend.js
msgid "*Start typing to select the product from the list."
msgstr ""

#: backend.js
msgid "*Only variable product will be shown in the product list."
msgstr ""

#: backend.js
msgid "Dedicated Support"
msgstr ""

#: backend.js
msgid "Our support is what makes us No 1. We are available for any support"
msgstr ""

#: backend.js
msgid "Contact Support"
msgstr ""

#: backend.js
msgid "Full Documentation"
msgstr ""

#: backend.js
msgid "Look for any helps in our documentation if you are stuck with anything"
msgstr ""

#: backend.js
msgid "Read More"
msgstr ""

#: backend.js
msgid "Type to Select Category"
msgstr ""

#: backend.js
msgid "No categories found"
msgstr ""

#: backend.js
msgid "No tags found"
msgstr ""

#: backend.js
msgid "Type to Select Tags"
msgstr ""

#: backend.js
msgid "Tag"
msgstr ""

#: backend.js
msgid "Select Type"
msgstr ""

#: backend.js
msgid "Limit"
msgstr ""

#: backend.js
msgid "Product List"
msgstr ""

#: backend.js
msgid "Discount"
msgstr ""

#: backend.js
msgid "Edit rule"
msgstr ""

#: backend.js
msgid "Add New"
msgstr ""

#: backend.js
msgid "Saving Changes..."
msgstr ""

#: backend.js
msgid "Product Rules"
msgstr ""

#: backend.js
msgid "Fixed price of product price"
msgstr ""

#: backend.js
msgid "Percentage of product price"
msgstr ""

#: backend.js
msgid "Cart Rules"
msgstr ""

#: backend.js
msgid "Fixed price of cart total amount"
msgstr ""

#: backend.js
msgid "Percentage of cart total amount"
msgstr ""

#: backend.js
msgid "Quantity Rules"
msgstr ""

#: backend.js
msgid "Quantity based discount"
msgstr ""

#: backend.js
msgid "Bogo Rules"
msgstr ""

#: backend.js
msgid "Buy X Get X"
msgstr ""

#: backend.js
msgid "Gift Product"
msgstr ""

#: backend.js
msgid "Discount on individual product quantity"
msgstr ""

#: backend.js
msgid "Discount on cart items count (different products count)"
msgstr ""

#: backend.js
msgid "Discount on cart quantity"
msgstr ""

#: backend.js
msgid "Vertical"
msgstr ""

#: backend.js
msgid "Horizontal"
msgstr ""

#: backend.js
msgid "Buy X get Y"
msgstr ""

#: backend.js
msgid "Buy X get X (Same Product)"
msgstr ""

#: backend.js
msgid "Buy X get discount on nth item"
msgstr ""

#: backend.js
msgid "Buy X get discount for n items"
msgstr ""

#: backend.js
msgid "Buy X get discount applied on the cheapest product in the cart"
msgstr ""

#: backend.js
msgid "Number of items in Cart (excluding free products)"
msgstr ""

#: backend.js
msgid "Cart Total"
msgstr ""

#: backend.js
msgid "Product Price"
msgstr ""

#: backend.js
msgid "Spent $$$ and get free gifts"
msgstr ""

#: backend.js
msgid "Buy from a Category and get free gift"
msgstr ""

#: backend.js
msgid "Free gift for First Order"
msgstr ""

#: backend.js
msgid "Gift for any products added in the Cart"
msgstr ""

#: backend.js
msgid "Gift Combinations"
msgstr ""

#: backend.js
msgid "All Days"
msgstr ""

#: backend.js
msgid "Sunday"
msgstr ""

#: backend.js
msgid "Monday"
msgstr ""

#: backend.js
msgid "Tuesday"
msgstr ""

#: backend.js
msgid "Wednesday"
msgstr ""

#: backend.js
msgid "Thursday"
msgstr ""

#: backend.js
msgid "Friday"
msgstr ""

#: backend.js
msgid "Saturday"
msgstr ""

#: backend.js
msgid "Allo Good day !!"
msgstr ""

#: backend.js
msgid ""
"Setting the discount rate is easy. Just play with the below settings and "
"watch the changes"
msgstr ""

#: backend.js
msgid "Rule Name"
msgstr ""

#: backend.js
msgid "Enter rule name"
msgstr ""

#: backend.js
msgid "Discount Status"
msgstr ""

#: backend.js
msgid "Discount Settings"
msgstr ""

#: backend.js
msgid "Schedule"
msgstr ""

#: backend.js
msgid "Rules and Restrictions"
msgstr ""

#: backend.js
msgid "Usage Limits"
msgstr ""

#: backend.js
msgid "Additional Settings"
msgstr ""

#: backend.js
msgid "Priority"
msgstr ""

#: backend.js
msgid "Enter discount priority"
msgstr ""

#: backend.js
msgid ""
"*Cart based rules will have the least priority and will be applied once the "
"product is added to the cart."
msgstr ""

#: backend.js
msgid "Apply discount only for registered customers."
msgstr ""

#: backend.js
msgid ""
"*Discounts will be applied as Coupon with Discount Label ( Settings -> "
"Discount Label )"
msgstr ""

#: backend.js
msgid "Quantity Based Discount"
msgstr ""

#: backend.js
msgid "Set Quantity Based Discounts on Cart items / Products"
msgstr ""

#: backend.js
msgid "Type"
msgstr ""

#: backend.js
msgid "Show pricing table"
msgstr ""

#: backend.js
msgid "Table layout"
msgstr ""

#: backend.js
msgid "Consider variations as single product"
msgstr ""

#: backend.js
msgid ""
"*If checked, quantity of all variations (of a product) in the cart will be "
"considered as a single product quantity"
msgstr ""

#: backend.js
msgid ""
"*Discount value display settings can be managed from Settings -> Pricing "
"Table -> Pricing Table Value Display"
msgstr ""

#: backend.js
msgid "BOGO Options"
msgstr ""

#: backend.js
msgid "Bogo Type"
msgstr ""

#: backend.js
msgid "Repeat for every Buy X Get X "
msgstr ""

#: backend.js
msgid ""
"*Eg: For Buy 1 Get 1 offer, the rule will repeat for Buy 2 Get 2, Buy 3 Get "
"3 etc"
msgstr ""

#: backend.js
msgid "Allow discount to be applied on all items from the list"
msgstr ""

#: backend.js
msgid "Offer Count (n)"
msgstr ""

#: backend.js
msgid "*Discount will be applied on the nth quantity of the selected product."
msgstr ""

#: backend.js
msgid "Possible rules: Buy 3 get 50% off on the 4th item "
msgstr ""

#: backend.js
msgid "( Offer Count: 4, Discount Type: % Off, Discount Value: 50 )"
msgstr ""

#: backend.js
msgid "Repeat for every nth purchase"
msgstr ""

#: backend.js
msgid ""
"*Discount will be applied to n (Discount Items Count) quantity of the "
"selected product. "
msgstr ""

#: backend.js
msgid "Possible rules: Buy 5 get 20% discount on 2 items "
msgstr ""

#: backend.js
msgid ""
"( Buy Unit Quantity: 5, Discount Items Count: 2, Discount Type: % Off, "
"Value: 20 )"
msgstr ""

#: backend.js
msgid "Buy"
msgstr ""

#: backend.js
msgid "Get"
msgstr ""

#: backend.js
msgid "*User needs to purchase "
msgstr ""

#: backend.js
msgid "+ products to get the discount."
msgstr ""

#: backend.js
msgid "*Discount will be applied to cheapest n (Get) products in the cart."
msgstr ""

#: backend.js
msgid "*Individual product quantity won't be considered."
msgstr ""

#: backend.js
msgid "Product Combinations"
msgstr ""

#: backend.js
msgid ""
"*Each combination will be considered as separate rule, buy X get X will be "
"applied for each combination."
msgstr ""

#: backend.js
msgid "*By default description will be displayed above Add to Cart button."
msgstr ""

#: backend.js
msgid "GIFT Options"
msgstr ""

#: backend.js
msgid "Gift Variations"
msgstr ""

#: backend.js
msgid "NEW"
msgstr ""

#: backend.js
msgid "Note:"
msgstr ""

#: backend.js
msgid "Only products with variations will be shown for selection"
msgstr ""

#: backend.js
msgid "Discount based on the amount spent by user (completed orders)"
msgstr ""

#: backend.js
msgid "Free Product"
msgstr ""

#: backend.js
msgid "Number of gifts items allowed in a single order"
msgstr ""

#: backend.js
msgid "Number of gifts allowed"
msgstr ""

#: backend.js
msgid ""
"*Each count will be considered as separate product or item (by default only "
"one quantity / product will be considered as gift)"
msgstr ""

#: backend.js
msgid "Automatically add gifts to the Cart"
msgstr ""

#: backend.js
msgid "Will work only if number of free product is one."
msgstr ""

#: backend.js
msgid "*By default mdescription will be displayed above Add to Cart button."
msgstr ""

#: backend.js
msgid "Discount percentage / amount"
msgstr ""

#: backend.js
msgid "Set Multiple Discount Values"
msgstr ""

#: backend.js
msgid "*To Set Multiple Discount Values"
msgstr ""

#: backend.js
msgid ""
"Navigate to Rules and Restrictions tab, Click on Add Group, Set the rule "
"actions and set the discount values"
msgstr ""

#: backend.js
msgid ""
"Each group will be considered as different, and make sure to set the group "
"relation to "
msgstr ""

#: backend.js
msgid ""
"*Discount field will not be visible on existing restrictions (old version), "
"please remove the existing restrictions and add new restrictions to set "
"discount values."
msgstr ""

#: backend.js
msgid "Disable discount if product is on sale"
msgstr ""

#: backend.js
msgid "Skip this rule if any other pricing rule is applied on product"
msgstr ""

#: backend.js
msgid "Schedule Discount Sale"
msgstr ""

#: backend.js
msgid "Schedule discount sales for upcoming dates"
msgstr ""

#: backend.js
msgid ""
"*Please set date / time based on your server / wordpress time settings. "
"Current Date / Time: "
msgstr ""

#: backend.js
msgid ""
"*To set schedules based on default wordpress timezone, please enable the "
"option from our settings."
msgstr ""

#: backend.js
msgid "Schedule Discount on Weekdays"
msgstr ""

#: backend.js
msgid "Set discounts on weekdays"
msgstr ""

#: backend.js
msgid "Days"
msgstr ""

#: backend.js
msgid "Select weekdays"
msgstr ""

#: backend.js
msgid "Start Time"
msgstr ""

#: backend.js
msgid "Pick Time"
msgstr ""

#: backend.js
msgid "End Time"
msgstr ""

#: backend.js
msgid ""
"*Please set time based on your server / wordpress time settings. Current "
"Time: "
msgstr ""

#: backend.js
msgid "*End time should be greater than start time."
msgstr ""

#: backend.js
msgid "Apply Rules"
msgstr ""

#: backend.js
msgid "Set Restrcitions with the provided options"
msgstr ""

#: backend.js
msgid "*Please note"
msgstr ""

#: backend.js
msgid ""
"Rules will be considered based on discount values, the one with the highest "
"discount value will be considered first."
msgstr ""

#: backend.js
msgid "Enable Limits"
msgstr ""

#: backend.js
msgid "Usage / Limit:"
msgstr ""

#: backend.js
msgid "Usage Limit Per Discount"
msgstr ""

#: backend.js
msgid "Set Usage Limit"
msgstr ""

#: backend.js
msgid "Value cannot be greater than total usage limit"
msgstr ""

#: backend.js
msgid "Usage Limit Per User"
msgstr ""

#: backend.js
msgid "Set User Usage Limit"
msgstr ""

#: backend.js
msgid "Daily Usage Limits"
msgstr ""

#: backend.js
msgid "Daily Discount Limit"
msgstr ""

#: backend.js
msgid "Daily Limit Per User"
msgstr ""

#: backend.js
msgid "User Role Restrictions"
msgstr ""

#: backend.js
msgid "Shortcode Settings"
msgstr ""

#: backend.js
msgid "# Use shortcode "
msgstr ""

#: backend.js
msgid " for discount listing."
msgstr ""

#: backend.js
msgid "Title"
msgstr ""

#: backend.js
msgid "Description"
msgstr ""

#: backend.js
msgid "Message"
msgstr ""

#: backend.js
msgid "Delete"
msgstr ""

#: backend.js
msgid "Publish"
msgstr ""

#: backend.js
msgid "Update"
msgstr ""

#: backend.js
msgid "All discount related datas will be removed."
msgstr ""

#: backend.js
msgid "Are you sure to proceed?"
msgstr ""

#: backend.js
msgid "No"
msgstr ""

#: backend.js
msgid "Yes"
msgstr ""

#: backend.js
msgid "Deleting.. Please Wait.."
msgstr ""

#: backend.js
msgid "No product rules found."
msgstr ""

#: backend.js
msgid "TITLE"
msgstr ""

#: backend.js
msgid "DISCOUNT TYPE"
msgstr ""

#: backend.js
msgid "VALUE"
msgstr ""

#: backend.js
msgid "PRIORITY"
msgstr ""

#: backend.js
msgid "SCHEDULE"
msgstr ""

#: backend.js
msgid "STATUS"
msgstr ""

#: backend.js
msgid "ACTIONS"
msgstr ""

#: backend.js
msgid "will be removed."
msgstr ""

#: backend.js
msgid "Are you sure to proceed."
msgstr ""

#: backend.js
msgid "Edit"
msgstr ""

#: backend.js
msgid "Pricing Rule"
msgstr ""

#: backend.js
msgid "Products Selection"
msgstr ""

#: backend.js
msgid "Dynamic Request"
msgstr ""

#: backend.js
msgid "List Name"
msgstr ""

#: backend.js
msgid "Enter Rule Name"
msgstr ""

#: backend.js
msgid "Select List Type"
msgstr ""

#: backend.js
msgid "Taxonomy Relation"
msgstr ""

#: backend.js
msgid "Select Combination Relation"
msgstr ""

#: backend.js
msgid "Taxonomy Combinations"
msgstr ""

#: backend.js
msgid "Exclude Products"
msgstr ""

#: backend.js
msgid "Type to Select Products"
msgstr ""

#: backend.js
msgid "Attributes Relation"
msgstr ""

#: backend.js
msgid "Attributes Combinations"
msgstr ""

#: backend.js
msgid "Products"
msgstr ""

#: backend.js
msgid "All related datas will be removed."
msgstr ""

#: backend.js
msgid "No product lists found."
msgstr ""

#: backend.js
msgid "TYPE"
msgstr ""

#: backend.js
msgid "DATE"
msgstr ""

#: backend.js
msgid "In product summary"
msgstr ""

#: backend.js
msgid "Before add to cart form"
msgstr ""

#: backend.js
msgid "Before variations from"
msgstr ""

#: backend.js
msgid "Before add to cart button"
msgstr ""

#: backend.js
msgid "After add to cart button"
msgstr ""

#: backend.js
msgid "After variations from"
msgstr ""

#: backend.js
msgid "After add to cart form"
msgstr ""

#: backend.js
msgid "Product meta start"
msgstr ""

#: backend.js
msgid "Product meta end"
msgstr ""

#: backend.js
msgid "After product summary"
msgstr ""

#: backend.js
msgid "Ascending"
msgstr ""

#: backend.js
msgid "Descending"
msgstr ""

#: backend.js
msgid "Discounted Price"
msgstr ""

#: backend.js
msgid "Both Discount Value and Price"
msgstr ""

#: backend.js
msgid "Left"
msgstr ""

#: backend.js
msgid "Right"
msgstr ""

#: backend.js
msgid "Carousel"
msgstr ""

#: backend.js
msgid "Product Listing"
msgstr ""

#: backend.js
msgid "Default (price range)"
msgstr ""

#: backend.js
msgid "Display the smallest variant price"
msgstr ""

#: backend.js
msgid "Display the largest variant price"
msgstr ""

#: backend.js
msgid "Use default size"
msgstr ""

#: backend.js
msgid "12px"
msgstr ""

#: backend.js
msgid "14px"
msgstr ""

#: backend.js
msgid "16px"
msgstr ""

#: backend.js
msgid "18px"
msgstr ""

#: backend.js
msgid "20px"
msgstr ""

#: backend.js
msgid "22px"
msgstr ""

#: backend.js
msgid "24px"
msgstr ""

#: backend.js
msgid "General Settings"
msgstr ""

#: backend.js
msgid "Pricing Table"
msgstr ""

#: backend.js
msgid "Gift / Bogo Settings"
msgstr ""

#: backend.js
msgid "Offer Description"
msgstr ""

#: backend.js
msgid "Badge Settings"
msgstr ""

#: backend.js
msgid "Timer Settings"
msgstr ""

#: backend.js
msgid "Shortcode"
msgstr ""

#: backend.js
msgid "Timezone Settings"
msgstr ""

#: backend.js
msgid "License Key"
msgstr ""

#: backend.js
msgid "Discount Label"
msgstr ""

#: backend.js
msgid "Default Text:"
msgstr ""

#: backend.js
msgid "Variable Product Price Display"
msgstr ""

#: backend.js
msgid "Select Position"
msgstr ""

#: backend.js
msgid "Applicable only for product based rules"
msgstr ""

#: backend.js
msgid "Hide Coupon Box"
msgstr ""

#: backend.js
msgid "Will hide coupon box from your cart / checkout pages"
msgstr ""

#: backend.js
msgid "Disable Pricing Rules if normal Coupon is applied"
msgstr ""

#: backend.js
msgid "Will auto remove plugin discount from your cart / checkout pages"
msgstr ""

#: backend.js
msgid "Show discount applied message on cart page"
msgstr ""

#: backend.js
msgid "Discount has been applied!"
msgstr ""

#: backend.js
msgid "Discount \"\" has been applied!"
msgstr ""

#: backend.js
msgid "Use"
msgstr ""

#: backend.js
msgid "for label name"
msgstr ""

#: backend.js
msgid "Message:"
msgstr ""

#: backend.js
msgid "Save Changes"
msgstr ""

#: backend.js
msgid "Pricing Table Settings"
msgstr ""

#: backend.js
msgid "Pricing Table Position"
msgstr ""

#: backend.js
msgid "Default:"
msgstr ""

#: backend.js
msgid "*Pricing table will be shown only on products page with variations."
msgstr ""

#: backend.js
msgid "Pricing Table Value Soting (Sort Based on Product Quantity Range)"
msgstr ""

#: backend.js
msgid "Select Order"
msgstr ""

#: backend.js
msgid "Pricing Table Value Display"
msgstr ""

#: backend.js
msgid "Select View"
msgstr ""

#: backend.js
msgid "Hide Pricing Table Value Text"
msgstr ""

#: backend.js
msgid "If enabled no texts will be shown after the value"
msgstr ""

#: backend.js
msgid "Pricing Table Value Text* (Suffix Text)"
msgstr ""

#: backend.js
msgid "Depends on Discount Type"
msgstr ""

#: backend.js
msgid "Pricing Table Title"
msgstr ""

#: backend.js
msgid "Table Title"
msgstr ""

#: backend.js
msgid "Quantity Discounts"
msgstr ""

#: backend.js
msgid "Price Label"
msgstr ""

#: backend.js
msgid "Price"
msgstr ""

#: backend.js
msgid "Quantity Label"
msgstr ""

#: backend.js
msgid "Quantity"
msgstr ""

#: backend.js
msgid "Price Label (Third Column Title)"
msgstr ""

#: backend.js
msgid "Font size"
msgstr ""

#: backend.js
msgid "If not selected, default font size will be used."
msgstr ""

#: backend.js
msgid "Border Color"
msgstr ""

#: backend.js
msgid "Cart Items Count table Description (shown below quantity table)"
msgstr ""

#: backend.js
msgid ""
" refers to discounted items (products with discount) individual count on "
"cart."
msgstr ""

#: backend.js
msgid "Cart Quantity table Description (shown below quantity table)"
msgstr ""

#: backend.js
msgid " refers to discounted items (products with discount) total quantity on cart."
msgstr ""

#: backend.js
msgid "Show Offer Products on Product Single Page (Will work only for gift items)"
msgstr ""

#: backend.js
msgid "Show Offer Products on Cart Page"
msgstr ""

#: backend.js
msgid "Offer Listing Title"
msgstr ""

#: backend.js
msgid "Eligible Free Gifts"
msgstr ""

#: backend.js
msgid "Offer Listing Description (Optional)"
msgstr ""

#: backend.js
msgid "Bogo Options"
msgstr ""

#: backend.js
msgid "Offer on following products"
msgstr ""

#: backend.js
msgid "Offer Description Display Settings (Single Page)"
msgstr ""

#: backend.js
msgid "Single Page Message Position"
msgstr ""

#: backend.js
msgid "Font Size"
msgstr ""

#: backend.js
msgid "Padding (Left/Right)"
msgstr ""

#: backend.js
msgid "Padding"
msgstr ""

#: backend.js
msgid "Padding (Top/Bottom)"
msgstr ""

#: backend.js
msgid "Border Radius"
msgstr ""

#: backend.js
msgid "Background Color"
msgstr ""

#: backend.js
msgid "Font Color"
msgstr ""

#: backend.js
msgid "Show Sale Badge on Products"
msgstr ""

#: backend.js
msgid "Note: "
msgstr ""

#: backend.js
msgid ""
"Sale badges will be shown only on products with product based discount "
"rules."
msgstr ""

#: backend.js
msgid "Hide Default Sale Tag (Woocommerce Sale Tag)"
msgstr ""

#: backend.js
msgid "Badge Label"
msgstr ""

#: backend.js
msgid "Sale"
msgstr ""

#: backend.js
msgid "Badge Position"
msgstr ""

#: backend.js
msgid "Only active pricing rules will be shown"
msgstr ""

#: backend.js
msgid "Badge Style"
msgstr ""

#: backend.js
msgid "Select the badge"
msgstr ""

#: backend.js
msgid "Badge Color"
msgstr ""

#: backend.js
msgid "Label Color"
msgstr ""

#: backend.js
msgid "Show Timer on Product Detail Page"
msgstr ""

#: backend.js
msgid "Timer Position"
msgstr ""

#: backend.js
msgid "Timer Labels"
msgstr ""

#: backend.js
msgid "Hrs"
msgstr ""

#: backend.js
msgid "Mins"
msgstr ""

#: backend.js
msgid "Secs"
msgstr ""

#: backend.js
msgid "Set Timer Start / End Dates"
msgstr ""

#: backend.js
msgid "If not set , selected pricing rules schedule will be used."
msgstr ""

#: backend.js
msgid "Start Date"
msgstr ""

#: backend.js
msgid "End Date"
msgstr ""

#: backend.js
msgid "Timer will be displayed only after the start date."
msgstr ""

#: backend.js
msgid "Timer Style"
msgstr ""

#: backend.js
msgid "Select a style"
msgstr ""

#: backend.js
msgid "Border / Background Color"
msgstr ""

#: backend.js
msgid "Timer Display"
msgstr ""

#: backend.js
msgid "Timer Text"
msgstr ""

#: backend.js
msgid "Use {timer} for displaying timer clock."
msgstr ""

#: backend.js
msgid "Apply Discount on Woocommerce Fee"
msgstr ""

#: backend.js
msgid ""
"By default woocommerce doesn't allow to apply Coupons on Fee, this work is "
"in progress, please get back us if you are find any issues with the same."
msgstr ""

#: backend.js
msgid "Shortcode Listing Style"
msgstr ""

#: backend.js
msgid "Paginate"
msgstr ""

#: backend.js
msgid "Products Per Page"
msgstr ""

#: backend.js
msgid "The number of products to display"
msgstr ""

#: backend.js
msgid "Columns"
msgstr ""

#: backend.js
msgid "No. of Columns"
msgstr ""

#: backend.js
msgid "The number of columns to display"
msgstr ""

#: backend.js
msgid "Tips:"
msgstr ""

#: backend.js
msgid "# Use shortcode"
msgstr ""

#: backend.js
msgid " for listing all active discount rules."
msgstr ""

#: backend.js
msgid ""
"# You can use discount rule id for displaying discounts related to specific "
"rule. Eg:"
msgstr ""

#: backend.js
msgid "# Multiple ids can also be used. Eg:"
msgstr ""

#: backend.js
msgid ""
"By default our plugin uses server timezone for schedules / timers, to use "
"the default Wordpress Timezone, please enable this option"
msgstr ""

#: backend.js
msgid "Use Wordpress Timezone"
msgstr ""

#: backend.js
msgid "Plugin License"
msgstr ""

#: backend.js
msgid "Status:"
msgstr ""

#: backend.js
msgid "Active"
msgstr ""

#: backend.js
msgid "Inactive"
msgstr ""