﻿=== WooCommerce Dynamic Pricing With Discount Rules ===
Author: Acowebs
Author URI: https://acowebs.com
Contributors: acowebs, acowebssupport, ijasacodez
Donate link:
Tags: WooCommerce Dynamic Pricing, WooCommerce Discount rules, WooCommerce Bulk Discounts, WooCommerce Pricing Deals
Requires at least: 4.4
Tested up to: 6.4
Stable tag: 5.1.8

WooCommerce Dynamic Pricing with Discount Rules plugin helps to apply bulk discounts to WooCommerce products quickly. Just install and go ahead and add your pricing deals based on any specified parameters, in a matter of minutes - its path-breaking UX design makes applying discounts or setting discount rules really effortless and fun.

== Description ==

WooCommerce Dynamic Pricing with Discount Rules plugin helps to apply bulk discounts to WooCommerce products quickly. Just install and go ahead and add your pricing deals based on any specified parameters, in a matter of minutes - its path-breaking UX design makes applying discounts or setting discount rules really effortless and fun.

== HOW TO APPLY BULK DISCOUNTS OR TO SET DISCOUNT RULES? ==
After activating WooCommerce Dynamic Pricing and Discount Rules plugin, go to the menu WooCommerce >> Pricing Rules. 
Here you can add new pricing or discount rules by clicking on the 'Add New Rule' button. (Please refer to our video and screenshots below for a better understanding.)

In the discount rules form, you can fill following fields:

<ul>
<li>Rule Name: Name for the discount rule - add any name of your choice.</li>
<li>Discount Label: It's the label that will be visible for customers in the Cart, Checkout and Order Page, which shows how much discount is being applied (It can be some thing like, 'New Year offer', 'Summer Sales' etc).</li>
<li>Priority: If there are multiple discount rules, you can set priority on which one to be applied first.</li>
<li>Product List: This is the list that you can create by going to WordPress backend >> WooCommerce >> 'Product Lists'. It's the list of product where you want to apply this discount rule. There are further parameters that you can select while creating a 'Product List'.</li>
<li>Discount Type: You can select the 'Discount Type' - that if to apply discount based on Cart Price or on the Product Price, or based on a Fixed price or a Percentage value.</li>
<li>Discount Value: It is this value that will be applied as discount. In case of Percentage Type, add the percentage value (Eg: 10 for 10%) and in case of Fixed Type, add the fixed price (Eg.120).</li>
</ul>

== CHECK OUT OUR VIDEO DEMO ON HOW THIS WORKS: ==
-Video-

== DISCOUNT PLUGIN FEATURES ==
✅ Apply discounts based on Cart Total Price.
✅ Apply discounts based on Product Price.
✅ Apply Percentage Discount based on Cart Total Amount and Product Price.
✅ Restrict or limit discount rules to some products.
✅ Apply discount only if cart or product meets certain criteria.

== DEMO - CHECK THIS OUT IN REAL ==

🔗 [Front end Demo](http://wdp-free.demo.acowebs.com/)
🔗 [Backend Demo](http://wdp-free.demo.acowebs.com/wp-admin/admin.php?page=awdp_admin_ui#/)

== INSTALLATION ==

Installing "Woocommerce dynamic pricing" can be done either by searching for "Aco WooCommerce Dynamic Pricing" via the "Plugins > Add New" screen in your WordPress dashboard, or by using the following steps:

1. Download the plugin via WordPress.org
1. Upload the ZIP file through the 'Plugins > Add New > Upload' screen in your WordPress dashboard
1. Activate the plugin through the 'Plugins' menu in WordPress

== Screenshots ==

1. Pricing Rules
2. Discount Settings
3. Scheduling Discount
4. Rules and Restrictions
5. Product Lists
6. Product List - Product Selection
7. Product List - Dynamic Selection


== Frequently Asked Questions ==


== Changelog ==
= 5.1.8 =
* Support for WordPress 6.4 and WooCommerce 8.6
* [Bug Fix]: Addons compatibility issue with quantity based dis rules.
= 5.1.7 =
* Added repeat option for 'Buy X get a discount applied on the cheapest product in the cart' rule
* [Bug Fix]: Admin order discount split issues
= 5.1.6 =
* [Bug Fix]: Rules & Restriction fix
= 5.1.5 =
* [Bug Fix]: User restriction issue
= 5.1.4 =
* Support for individual quantity for BOGO Cheapest Rule
* Added User role restriction for discount rules
* Added Realtime price / total price display for quantity rules
* Tiered pricing support
* Discount description UI settings update
* Added option to add custom cart message
* Support for WooCommerce 7.5 and WordPress 6.2
= 5.1.1 =
* UI Update
= 5.1.0 =
* Support for WooCommerce 7.3 and WordPress 6.1
= 5.0.9 =
* [Bug Fix]: BOGO not getting applied to addon products
* [Bug Fix]: Polylang conflict fix
= 5.0.8 =
* [Bug Fix]: WPML currency compatibility fix
= 5.0.7 =
* [Bug Fix]: BOGO Fix when multiple rules are active
= 5.0.6 =
* Added support for WooCommerce 7.1
= 5.0.5 =
* Admin orders update
* CURCY - WooCommerce Multi Currency support
* [Bug Fix]: PHP Warning
* [Bug Fix]: Pricing Table price issue when tax settings is set to prices exclusive of tax 
= 5.0.4 =
* Added individual quantity check for BOGO Buy X Get Y rules
* Added support for applying discount to multiple products for BOGO Buy X Get X rules
* Added support for woocommerce minicart 
* Support for WooCommerce 6.9
* [Bug Fix]: BOGO Buy X get discount for n items discount issue on variable products
* [Bug Fix]: 'Variable Product Price Display' option not reflecting on product page 
* [Bug Fix]: Addons compatibility fix
= 5.0.3 =
* Support for WooCommerce 6.6 and WordPress 6.0
* Added multiple discount options for product based rules
* Added Cart Quantity (Product List) option in Rules and Restrictions
* Added option to disable discount if any other coupon is applied to the cart
* UI Update
* [Bug Fix]: Timer not working on safari
* [Bug Fix]: Quantity based discount not getting applied when product price is 0
* [Bug Fix]: BOGO Rules 'Discount on Sale items' issue
= 5.0.2 =
* Support for WooCommerce 6.3
= 5.0.1 =
* Support for WordPress 5.9 and WooCommerce 6.2
* JetWoobuilder compatibility fix
* Added option to use Wordpress Timezone for schedules
= 5.0.0 =
* Support for WooCommerce 6.1
* Added fixed discount option for BOGO rules
* Removed logged in check for User / User Role Restrictions
* [Bug Fix]: Warning fix
* [Bug Fix]: Variable price view issue
= 4.0.4 =
* Sale Badges: added option to select a rule
* Support for WordPress 5.8.1
* Added WPML Support for Pricing Table
= 4.0.3 =
* [Bug Fix]: Product table plugin conflict
= 4.0.2 =
* Support for WordPress 5.8.0
* [Bug Fix]: Pricing table admin error fix
* [Bug Fix]: Rules and restriction not getting applied
= 4.0.1 =
* [Bug Fix]: Product table display issue
* [Bug Fix]: Product detail page display issue
= 4.0.0 =
* Major update (discount calculation method optimized for faster loading)
* Compatibility issues fix
* Added support for Acowebs Custom Products Addons Plugin
* Support for Woocomerce 5.5.1 and WordPress 5.7.2
* Added support for PHP 8.x.x
* [Bug Fix]: Rest API Warnings 
= 3.4.0 =
* Added variation selection for product list
* Support for WooCommerce 5.0.0 and WordPress 5.6.1
= 3.3.7 =
* Support for WooCommerce 4.7.1
= 3.3.6 =
* [Bug Fix]: Warning Fix
= 3.3.5 =
* Support for WooCommerce 4.7.0 and WordPress 5.5.3
* [Bug Fix]: Product List selection not working on 'Buy X get X discount applied on cheapest product'
= 3.3.4 =
* Support for WordPress 5.5.1 and WooCommerce 4.5.1
* [Bug Fix]: Shortcode bug fix
* [Bug Fix]: Discount not geeting applied when discount label have special characters
= 3.3.3 =
* [Bug Fix]: Warning Fix
= 3.3.2 =
* Option added for skipping pricing rules if already applied
* [Bug Fix]: Shortcode not displaying bogo products
= 3.3.1 =
* Support for WordPress 5.5.0 and WooCommerce 4.4.1
* [Bug Fix]: Product list not working in multilingual sites ( WPML ) 
= 3.3.0 =
* UI enhancements
* Support for Woocommerce 4.3.1
* [Bug Fix]: Discount getting applied to all products when Product List Type is not selected
= 3.2.1 =
* Added discount daily limit options
* Restricted plugin css to load only on plugin related pages
* UI enhancements
* [Bug Fix]: Tax fix
= 3.1.7 =
* Added separate timepicker for Schedule
* UI enhancements
* Support for WordPress 5.4.1 & WooCommerce 4.1.0
* [Bug Fix]: Gift section
= 3.1.6 =
* [Bug Fix]: Schdeduler not working on safari web browser
* [Bug Fix]: Bogo rule category selection fix
= 3.1.5 =
* Performance enhancements
* Added new feature for quantity type discount, now all variations of a product can be considered as a single product quantity.
* [Bug Fix]: Discount applied message appears everytime when a products gets aadded to the cart
* Support for WordPress 5.4
= 3.1.2 =
* Support for Woocommerce 4
* [Bug Fix]: Discount not getting applied on sale price
= 3.1.1 =
* Performance enhancements
* Buy X Get X - Repeat option added for buy x get x rules (Buy X Get Y and Buy X Get X -Same Product) 
* [Bug Fix]: Sale badges not getting displayed on some themes
= 3.1.0 =
* Added countdown timer for discounts
* Added option to set discount based on payment methods / shipment methods
* Added option for changing pricing table border color
* Added option to restrict discount rules for registered customers
* Added new options for Bogo rules - option to apply discount to nth item, discount for n items, cheapest product.
* Set discount to a product if users previous order contains a selected product
* Now pricing table can display both value and price at the same time
* [Bug Fix]: Variable products price strikeout not getting displayed
= 3.0.8 =
* Added compatibility with Woocommerce Multi Currency Plugin
= 3.0.7 =
* [Bug Fix]: Variable product strikeout issue
* [Bug Fix]: Bogo cart display price fix
= 3.0.6 =
* Added user selection, now you can set discounts for selected users
* [Bug Fix]: Bogo - Variable products
= 3.0.5 =
* Added sale badges
* Added support for addons plugin
* Added a new option for quantity based discounts
= 3.0.4 =
* Added pricing table for cart quantity discounts
* Now cart quantity discounts shows the individual discount on cart items
* [Bug Fix]: Pricing table value when tax is enabled
* [Bug Fix]: Gift getting added more than once
* [Bug Fix]: Bogo - Buy X get X fix
= 3.0.3 =
* [Bug Fix]: Quantity discount showing 0 as product price on cart
= 3.0.2 =
* [Bug Fix]: Checkout discount issue
= 3.0.1 =
* [Bug Fix]: Warning fix (notice)
* [Bug Fix]: Bogo rules not getting applied
= 3.0.0 =
* 2019-10-31
* Pro-version release

== Upgrade Notice ==
