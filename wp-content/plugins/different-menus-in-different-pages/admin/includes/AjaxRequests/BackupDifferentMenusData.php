<?php
namespace DMIDP\AjaxRequests;

class BackupDifferentMenusData
{

    private $ajax;

    /**
     * PreloadCaches constructor.
     */
    public function __construct($a)
    {
        $this->ajax = $a;
        add_action("wp_ajax_backup_different_menus_data", [$this, 'ajax']);
    }

    public function ajax()
    {
        $settings_data = get_option('different_menus_for_different_page');
        $settings = array();

        $x = 0;
        if (!empty($settings_data)) {
            foreach ($settings_data as $theme => $locationData) {
                foreach ($locationData as $location => $menuData) {
                    foreach ($menuData as $assigned_menu => $menu) {
                        $theme_location = $location;
                        $condition = $menu['name'][0]; // Extracting the menu name from the array
                        $x++;
                        $settings[] = (object) array(
                            'id' => strval($x),
                            'location' => $theme_location,
                            'assined_menu' => str_replace('menu_', '', $assigned_menu),
                            'menu_condition' => $condition,
                        );
                    }
                }
            }
        }

        echo serialize($settings);

        die();
    }
}
