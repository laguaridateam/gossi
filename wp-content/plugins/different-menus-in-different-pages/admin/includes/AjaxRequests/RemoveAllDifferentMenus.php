<?php
namespace DMIDP\AjaxRequests;

class RemoveAllDifferentMenus
{

    private $ajax;

    /**
     * PreloadCaches constructor.
     */
    public function __construct($a)
    {
        $this->ajax = $a;
        add_action("wp_ajax_remove_all_different_menus", [$this, 'ajax']);
    }

    public function ajax()
    {
        if(check_ajax_referer( 'recorp_different_menu', 'nonce' )){
            delete_option('different_menus_for_different_page');
        }
        die();
    }
}
