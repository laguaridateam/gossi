<?php
namespace DMIDP\AjaxRequests;

class RemoveDifferentMenu
{

    private $ajax;

    /**
     * PreloadCaches constructor.
     */
    public function __construct($a)
    {
        $this->ajax = $a;
        add_action("wp_ajax_remove_different_menu", [$this, 'ajax']);
    }

    public function ajax()
    {
        if(check_ajax_referer( 'recorp_different_menu', 'nonce' )){

            $theme_location = sanitize_key($_POST['theme_location']);
            $assigned_menu = sanitize_text_field($_POST['assigned_menu']);
            $assigned_menu = "menu_" . $assigned_menu;

            $settings = get_option('different_menus_for_different_page');

            unset($settings[get_stylesheet()][$theme_location][$assigned_menu]);

            update_option('different_menus_for_different_page', $settings);
        }

        die();
    }
}
