== Description ==

Integracion de Enviopack para tu tienda de WooCommerce

== Screenshots ==

Podes ver algunas capturas del plugin funcionando en /screenshots

== Installation ==

1. Subi los archivos del plugin a la carpeta `/wp-content/plugins/woocommerce-enviopack`, o instala el plugin a traves de WordPress plugins directamente.
2. Activa el plugin a traves de la pantalla de 'Plugins' en WordPress
3. Completa los ajustes necesarios desde Ajustes -> Configuración de EnvíoPack
4. Usa el plugin en la configuracion de Envios y Zonas de Woocommerce

== Help ==

Si necesitas ayuda para la integracion contactanos a traves de nuestro contact center en https://app.enviopack.com/contact-center