(function( $ ) {
	'use strict';

	// Create the defaults once
	var pluginName = "WooCommerceShopLook",
		defaults = {
			'modalHeightAuto' : '1',
			'loadingSVG' : '<svg width="40" height="10" viewBox="0 0 120 30" xmlns="http://www.w3.org/2000/svg" fill="#fff"> <circle cx="15" cy="15" r="15"> <animate attributeName="r" from="15" to="15" begin="0s" dur="0.8s" values="15;9;15" calcMode="linear" repeatCount="indefinite" /> <animate attributeName="fill-opacity" from="1" to="1" begin="0s" dur="0.8s" values="1;.5;1" calcMode="linear" repeatCount="indefinite" /> </circle> <circle cx="60" cy="15" r="9" fill-opacity="0.3"> <animate attributeName="r" from="9" to="9" begin="0s" dur="0.8s" values="9;15;9" calcMode="linear" repeatCount="indefinite" /> <animate attributeName="fill-opacity" from="0.5" to="0.5" begin="0s" dur="0.8s" values=".5;1;.5" calcMode="linear" repeatCount="indefinite" /> </circle> <circle cx="105" cy="15" r="15"> <animate attributeName="r" from="15" to="15" begin="0s" dur="0.8s" values="15;9;15" calcMode="linear" repeatCount="indefinite" /> <animate attributeName="fill-opacity" from="1" to="1" begin="0s" dur="0.8s" values="1;.5;1" calcMode="linear" repeatCount="indefinite" /> </circle></svg>',
			'addAllToCartButton' : '.woocommerce-shop-look-add-all-to-cart-button',
			'addToCartCheckboxes' : '.woocommerce-shop-look-item-checkbox',
			'addAllToCartCount' : '.woocommerce-shop-look-add-all-to-cart-count',
			'addAllToCartTotal' : '.woocommerce-shop-look-add-all-to-cart-total',
		};

	// The actual plugin constructor
	function Plugin ( element, options ) {
		this.element = element;
		
		this.settings = $.extend( {}, defaults, options );
		this._defaults = defaults;
		this.trans = this.settings.trans;
		this._name = pluginName;
		this.init();
	}

	// Avoid Plugin.prototype conflicts
	$.extend( Plugin.prototype, {
		init: function() {
			this.window = $(window);
			this.documentHeight = $( document ).height();
			this.windowHeight = this.window.height();
			this.product = {};
			this.elements = {};

			this.checkboxProducts = {};
			
			this.variationSelect();
			this.addToCart();
			this.addAllToCart();
		},
		variationSelect : function() {

			var that = this;
			var possibleVariations;
			var possibleVariationsJSON;

			$(document).on('change', '.woocommerce-shop-look-item-cart select', function(e) {
				var $this = $(this);
				var itemContainer = $this.parents('.woocommerce-shop-look-item');
				var itemContainerSelects = itemContainer.find('select');
				
				var allHaveValues = true;
				var selectedAttributes = {};
				$.each(itemContainerSelects, function(i, index) {

					var itemContainerSelectName = $(this).attr('name');
					var itemContainerSelectValue = $(this).val();
					if(!itemContainerSelectValue || itemContainerSelectValue == "") {
						allHaveValues = false;
						return;
					}

					selectedAttributes[itemContainerSelectName] = itemContainerSelectValue;

				});


				if(allHaveValues) {

					possibleVariations = itemContainer.find('input[name="possible_variations"]').val();
					possibleVariationsJSON = JSON.parse( possibleVariations );
					
					if(!possibleVariationsJSON) {
						return;
					}

					$.each(selectedAttributes, function(selectedAttributeKey, selectedAttributeValue) {

						$.each(possibleVariationsJSON, function(possibleVariationKey, possibleVariation) {
							if(!possibleVariation) {
								return;
							}

							$.each(possibleVariation.attributes, function(possibleVariationAttributeKey, possibleVariationAttributeValue) {
								if(!possibleVariation.attributes.hasOwnProperty(selectedAttributeKey)) {
									delete(possibleVariationsJSON[possibleVariationKey]);
									return;
								}

								if(possibleVariation.attributes[selectedAttributeKey] !== "" && possibleVariation.attributes[selectedAttributeKey] != selectedAttributeValue) {
									delete(possibleVariationsJSON[possibleVariationKey]);
									return;
								}
							});
						});
					});

					possibleVariationsJSON = possibleVariationsJSON.filter(Boolean);
					if(possibleVariationsJSON.length < 1) {
						itemContainer.find('.woocommerce-shop-look-item-stock-status').html('');
						itemContainer.find('button').attr('disabled', true);
						return;
					}

					$.each(possibleVariationsJSON, function(i, variation) {
					    
					    if(!variation) {
					    	return;
					    }
					  				
						if(!variation.variation_id) {
							return;
						}

						itemContainer.find('.woocommerce-shop-look-item-checkbox').val(variation.variation_id);
						itemContainer.find('input[name="variation_id"]').val(variation.variation_id);

						if(variation.price_html == "") {
							itemContainer.find('.woocommerce-shop-look-item-price').html(variation.display_price + ' ' + that.settings.currencySymbol).data('price', variation.display_price);
						} else {
							itemContainer.find('.woocommerce-shop-look-item-price').html(variation.price_html).data('price', variation.display_price);
						}

						if(variation.image) {
							itemContainer.find('.woocommerce-shop-look-item-image img').attr('src', variation.image.full_src);
						}
						
						if(!variation.is_in_stock) {
							itemContainer.find('button').attr('disabled', true);
							itemContainer.find('.woocommerce-shop-look-item-checkbox').attr('disabled', true);
							itemContainer.find('.woocommerce-shop-look-item-stock-status').html(variation.availability_html);
						} else {
							itemContainer.find('button').attr('disabled', false);
							itemContainer.find('.woocommerce-shop-look-item-checkbox').attr('disabled', false);
							itemContainer.find('.woocommerce-shop-look-item-stock-status').html(variation.availability_html);
						}

						that.updateCheckboxProducts();

						return false;
					});
				} else {
					itemContainer.find('button').attr('disabled', true);
				}
			});
		},
		// Add Products to Cart
		addToCart : function()	{
			
			var that = this;

			$(document).on('submit', '.woocommerce-shop-look-item-cart', function(e) {
				e.preventDefault();


				var $this = $(this);
				var button = $this.find('button');
				var buttonTextOriginal = button.html();

				button.html(that.settings.loadingSVG);

				var data = $this.serialize();

				$.post( that.settings.ajax_url, data, function( response ) {
					
					if ( ! response ) {
						return;
					}
					
					if( response.error ) {
						if(response.error[0].notice) {
							alert(response.error[0].notice);
						}
					}

					$( document.body ).trigger( 'added_to_cart', [ response.fragments, response.cart_hash, button ] );

					button.html('✓');

					if ( wc_add_to_cart_params.cart_redirect_after_add === 'yes' ) {
						window.location = wc_add_to_cart_params.cart_url;
						return;
					}

					setTimeout( function(){ button.html(buttonTextOriginal); }, 5000);
				}, 'json' );

			});
		},
		updateCheckboxProducts : function() {

			var that = this;
			var total = 0;
			that.checkboxProducts = $(".woocommerce-shop-look-item-checkbox:checked").map(function() {

				var $this = $(this);
				var itemContainer = $this.parents('.woocommerce-shop-look-item');
				var button = itemContainer.find('button');
				if(button.is(":disabled")) {
					return;
				}

				var price = $this.parents('.woocommerce-shop-look-item').find('.woocommerce-shop-look-item-price').data('price');
				if(price) {
					total = total + price;
				}
			 	return $this.val();
			}).get();

			$(that.settings.addAllToCartCount).text(that.checkboxProducts.length);
			$(that.settings.addAllToCartTotal).text( that.numberWithCommas( total.toFixed(2) ) );

			if(that.checkboxProducts.length > 0) {
				$(that.settings.addAllToCartButton).attr('disabled', false);
			} else {
				$(that.settings.addAllToCartButton).attr('disabled', true);
			}
		},
		addAllToCart : function() {

			var that = this;

			$(document).on('change', that.settings.addToCartCheckboxes, function(i, index) {
				that.updateCheckboxProducts();
			});


			var products;
			$(document).on('submit', '.woocommerce-shop-look-add-all-to-cart', function(e) {

				e.preventDefault();

				var $this = $(this);
				var button = $this.find('button');
				var buttonTextOriginal = button.html();

				button.html(that.settings.loadingSVG);

				if(that.checkboxProducts.length < 1) {
					alert('No Products selected');
				}

				var data = {
					'action' : 'woocommerce_shop_look_add_all_to_cart',
					'products' : that.checkboxProducts
				};
				

				$.post( that.settings.ajax_url, data, function( response ) {
					
					if ( ! response ) {
						return;
					}

					$( document.body ).trigger( 'added_to_cart', [ response.fragments, response.cart_hash, button ] );

					button.html('✓');

					if ( wc_add_to_cart_params.cart_redirect_after_add === 'yes' ) {
						window.location = wc_add_to_cart_params.cart_url;
						return;
					}

					setTimeout( function(){ button.html(buttonTextOriginal); }, 5000);
				});
			});
		},
		numberWithCommas : function (x) {
		    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, this.settings.thousandsSeperator);
		},
	} );

	// Constructor wrapper
	$.fn[ pluginName ] = function( options ) {
		return this.each( function() {
			if ( !$.data( this, "plugin_" + pluginName ) ) {
				$.data( this, "plugin_" +
					pluginName, new Plugin( this, options ) );
			}
		} );
	};

	$(document).ready(function() {

		$( "body" ).WooCommerceShopLook( 
			woocommerce_shop_look_options
		);

	} );

})( jQuery );