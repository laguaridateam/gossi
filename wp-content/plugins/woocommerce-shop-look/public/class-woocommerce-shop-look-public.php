<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://woocommerce.welaunch.io
 * @since      1.0.0
 *
 * @package    WooCommerce_Shop_Look
 * @subpackage WooCommerce_Shop_Look/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    WooCommerce_Shop_Look
 * @subpackage WooCommerce_Shop_Look/public
 * @author     Daniel Barenkamp <support@welaunch.io>
 */
class WooCommerce_Shop_Look_Public extends WooCommerce_Shop_Look {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	protected $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of this plugin.
	 */
	protected $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) 
	{
		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->shopTheLookProductIds = array();
	}

	/**
	 * Enqueue Styles
	 * @author Daniel Barenkamp
	 * @version 1.0.0
	 * @since   1.0.0
	 * @link    http://woocommerce.welaunch.io
	 * @return  boolean
	 */
	public function enqueue_styles()
	{
		global $woocommerce_shop_look_options;
		$this->options = $woocommerce_shop_look_options;

		if (!$this->get_option('enable')) {
			return false;
		}

		if($this->get_option('performanceOnlyWooPages') && !is_product()) {
			return;
		}

		wp_enqueue_style($this->plugin_name. '-public', plugin_dir_url(__FILE__). 'css/woocommerce-shop-look-public.css', array(), $this->version, 'all');
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 * @author Daniel Barenkamp
	 * @version 1.0.0
	 * @since   1.0.0
	 * @link    http://woocommerce.welaunch.io
	 * @return  boolean
	 */
	public function enqueue_scripts()
	{
		global $woocommerce_shop_look_options;

		$this->options = $woocommerce_shop_look_options;

		if (!$this->get_option('enable')) {
			return false;
		}

		if($this->get_option('performanceOnlyWooPages') && !is_product()) {
			return;
		}

		wp_enqueue_script($this->plugin_name. '-public', plugin_dir_url(__FILE__). 'js/woocommerce-shop-look-public.js', array('jquery'), $this->version, true);

        $forJS['ajax_url'] = admin_url('admin-ajax.php');
        $forJS['decimalsSeperator'] = wc_get_price_decimal_separator();
        $forJS['thousandsSeperator'] = wc_get_price_thousand_separator();
        $forJS['currencySymbol'] = get_woocommerce_currency_symbol();
        
        $forJS['variation_no_attribute_selected'] = esc_attr__( 'Please select some product options before adding this product to your cart.', 'woocommerce' );
        wp_localize_script($this->plugin_name . '-public', 'woocommerce_shop_look_options', $forJS);
	}

    /**
     * Init the Bought together
     * @author Daniel Barenkamp
     * @version 1.0.0
     * @since   1.0.0
     * @link    https://www.welaunch.io
     * @return  [type]                       [description]
     */
    public function init()
    {
        global $woocommerce_shop_look_options;

        $this->options = $woocommerce_shop_look_options;

		if (!$this->get_option('enable')) {
			return false;
		}

		$position = $this->get_option('displayPosition');
		$priority = $this->get_option('displayPriority');

		add_action($position, array($this, 'display_shop_look'), $priority);

		if($this->get_option('removeDefaultAddToCart')) {
			add_action( 'woocommerce_before_shop_loop_item', array($this,  'hide_cart_and_price' ) );
			add_action( 'woocommerce_before_single_product', array($this,  'hide_cart_and_price' ) );
		}

	}

	public function hide_cart_and_price()
	{
		global $post;

		$this->shopTheLookProductIds = get_post_meta($post->ID, 'woocommerce_shop_look_products', true);
		if(empty($this->shopTheLookProductIds)) {
			return;
		}

		// Remove Cart
		remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );

		// Remove Price
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
		remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
	}

	public function display_shop_look($customProduct = false)
	{
		if($customProduct) {
			$product = $customProduct;
		} else {
			global $product;	
		}

		if(!$product) {
			return;
		}

		$this->shopTheLookProductIds = get_post_meta($product->get_id(), 'woocommerce_shop_look_products', true);
		if(empty($this->shopTheLookProductIds)) {
			return;
		}


		echo '<div id="woocommerce-shop-look-container" class="woocommerce-shop-look-container">';

		?>
		<style>
			.woocommerce-shop-look-container button {
				background-color: <?php echo $this->get_option('buttonBackgroundColor') ?>;
				color: <?php echo $this->get_option('buttonTextColor') ?>;
			}

			.woocommerce-shop-look-item-cart-variation-attribute {
				border-color: <?php echo $this->get_option('buttonBackgroundColor') ?>;
			}

			.woocommerce-shop-look-container {
				color: <?php echo $this->get_option('textColor') ?>;
			}
		</style>

		<?php

		$textsAddToCart = $this->get_option('textsAddToCart');
		$textsAddAllToCart = $this->get_option('textsAddAllToCart');
		$textsProductsChoosen = $this->get_option('textsProductsChoosen');

		$currencyPosition = get_option('woocommerce_currency_pos');
		$currencySymbol = get_woocommerce_currency_symbol();

		foreach ($this->shopTheLookProductIds as $shopTheLookProductId) {

			$shopTheLookProduct = wc_get_product($shopTheLookProductId);
			if(!$shopTheLookProduct) {
				continue;
			}

			$shopTheLookProductLink = $shopTheLookProduct->get_permalink();
			$shopTheLookProductName = $shopTheLookProduct->get_name();
			$shopTheLookProductShortDescription = $shopTheLookProduct->get_short_description();
			$shopTheLookProducCategories = wc_get_product_category_list($shopTheLookProductId);
			$shopTheLookProductPrice = $shopTheLookProduct->get_price_html();
			$shopTheLookProductImage = wp_get_attachment_image_src( get_post_thumbnail_id( $shopTheLookProductId ), 'large' );
			$shopTheLookProductImage = (isset($shopTheLookProductImage[0]) && !empty($shopTheLookProductImage[0])) ?  $shopTheLookProductImage[0] : wc_placeholder_img_src();

			$shopTheLookProductAvailability = '';
			if($this->get_option('displayStock')) {
				$shopTheLookProductAvailability = $shopTheLookProduct->get_availability()['availability'];
			} 
			?>

			<div class="woocommerce-shop-look-item">

				<?php if($this->get_option('displayAddAllToCart')) { ?>
				<div class="woocommerce-shop-look-item-checkbox-container">

					<?php if($shopTheLookProduct->is_type('variable') || !$shopTheLookProduct->is_in_stock() || !$shopTheLookProduct->is_purchasable() ) { ?> 
					<input name="woocommerce_shop_look_items[]" type="checkbox" value="<?php echo $shopTheLookProductId ?>" disabled="disabled" class="woocommerce-shop-look-item-checkbox">
					<?php } else { ?>
					<input name="woocommerce_shop_look_items[]" type="checkbox" value="<?php echo $shopTheLookProductId ?>" class="woocommerce-shop-look-item-checkbox">
					<?php } ?>

				</div>
				<?php } ?>

				<?php if($this->get_option('displayImage')) { ?>
				<div class="woocommerce-shop-look-item-image">
					<a href="<?php echo $shopTheLookProductLink ?>">
						<img src="<?php echo $shopTheLookProductImage ?>" alt="<?php echo $shopTheLookProductName ?> ">
					</a>
				</div>
				<?php } ?>

				<div class="woocommerce-shop-look-item-info">

					<?php if($this->get_option('displayCategories')) { ?>
						<span class="woocommerce-shop-look-item-categories"><?php echo $shopTheLookProducCategories ?></span>
					<?php } ?>

					<?php if($this->get_option('displayTitle')) { ?>
						<span class="woocommerce-shop-look-item-title"><?php echo $shopTheLookProductName ?></span>
					<?php } ?>

					<?php if($this->get_option('displayPrice')) { ?>
						<span class="woocommerce-shop-look-item-price" data-price="<?php echo wc_get_price_to_display($shopTheLookProduct) ?>"><?php echo $shopTheLookProductPrice ?></span>
					<?php } ?>


					<?php if($this->get_option('displayShortDescription')) { ?>
						<div class="woocommerce-shop-look-item-short-description"><?php echo $shopTheLookProductShortDescription ?></div>
					<?php } ?>

	
					<?php if($this->get_option('displayAddToCart')) { ?>
					<form action="" method="POST" class="woocommerce-shop-look-item-cart">

						<input type="hidden" name="product_id" value="<?php echo $shopTheLookProductId ?>">
						<input type="hidden" name="variation_id" value="">
						<input type="hidden" name="quantity" value="1">
						<input type="hidden" name="action" value="woocommerce_shop_look_add_to_cart">
						<?php 

						if($shopTheLookProduct->is_type('variable')) {

							$variableHTML = "";
							$shopTheLookProductVariationAttributes = $shopTheLookProduct->get_variation_attributes();
							$shopTheLookProductVariations = $shopTheLookProduct->get_available_variations();

							$shopTheLookProductVariations = wp_json_encode( $shopTheLookProductVariations );
							$shopTheLookProductVariations = function_exists( 'wc_esc_json' ) ? wc_esc_json( $shopTheLookProductVariations ) : _wp_specialchars( $shopTheLookProductVariations, ENT_QUOTES, 'UTF-8', true );

							echo '<input type="hidden" name="possible_variations" value="' . $shopTheLookProductVariations . '">';


							echo '<div class="woocommerce-shop-look-item-cart-select-container">';

								if(!empty($shopTheLookProductVariationAttributes)) {
									foreach ($shopTheLookProductVariationAttributes as $shopTheLookProductVariationAttributeKey => $shopTheLookProductVariationAttributeValues) {

										if(empty($shopTheLookProductVariationAttributeValues)) {
											continue;
										}

										// Is Attribute Term
										if (strpos($shopTheLookProductVariationAttributeKey, 'pa_') !== FALSE){
											$shopTheLookProductVariationAttributeName = wc_attribute_label($shopTheLookProductVariationAttributeKey);
											$isAttributeTerm = true;
										// CUstom Attribute
										} else {
											$isAttributeTerm = false;
											$shopTheLookProductVariationAttributeName = $shopTheLookProductVariationAttributeKey;
											$shopTheLookProductVariationAttributeKey = strtolower( trim( preg_replace('/[^A-Za-z0-9-]+/', '-', $shopTheLookProductVariationAttributeKey) ) );

										}

										
										$variableHTML .= '<select name="attribute_' . strtolower( urlencode( $shopTheLookProductVariationAttributeKey ) ) . '" class="woocommerce-shop-look-item-cart-variation-attribute">';
											$variableHTML .= '<option value="">' . $shopTheLookProductVariationAttributeName . '</option>';
															
											foreach ($shopTheLookProductVariationAttributeValues as $shopTheLookProductVariationAttributeValue) {

												if ($isAttributeTerm){
													$shopTheLookProductVariationAttribute = get_term_by('slug', $shopTheLookProductVariationAttributeValue, $shopTheLookProductVariationAttributeKey);
													
													if(!$shopTheLookProductVariationAttribute) {
														continue;
													}

													$shopTheLookProductVariationAttributeName = $shopTheLookProductVariationAttribute->name;
												} else {
												    $customAttributes = maybe_unserialize( get_post_meta( $shopTheLookProductId, '_product_attributes' ) );
												    $shopTheLookProductVariationAttributeName = $shopTheLookProductVariationAttributeValue;
												}

												if(!$shopTheLookProductVariationAttributeName) {
													continue;
												}

												$variableHTML .= '<option value="' . $shopTheLookProductVariationAttributeValue . '">' . $shopTheLookProductVariationAttributeName . '</option>';
											}

										$variableHTML .= '</select>';
									}

									echo $variableHTML;
								echo '</div>';
							}

							echo '<div class="woocommerce-shop-look-item-cart-button-container woocommerce-shop-look-item-cart-button-variable-container ">';

								echo '<button class="btn button btn-primary woocommerce-shop-look-item-cart-button" disabled="disabled">' . $textsAddToCart . '</button>';
								echo '<span class="woocommerce-shop-look-item-stock-status">' . $shopTheLookProductAvailability . '</span>';

							echo '</div>';
						} else {
							echo '<div class="woocommerce-shop-look-item-cart-button-container">';

								if(!$shopTheLookProduct->is_in_stock() || !$shopTheLookProduct->is_purchasable()) {
									echo '<button class="btn button btn-primary woocommerce-shop-look-item-cart-button" disabled="disabled">' . $textsAddToCart . '</button>';
								} else {
									echo '<button class="btn button btn-primary woocommerce-shop-look-item-cart-button">' . $textsAddToCart . '</button>';
								}
								echo '<span class="woocommerce-shop-look-item-stock-status">' . $shopTheLookProductAvailability . '</span>';

							echo '</div>';
						}

						echo '<div class="woocommerce-shop-look-clear"></div>';

						?>
						
					</form>

					<?php } ?>

				</div>
				<div class="woocommerce-shop-look-clear"></div>
			</div>
			
		<?php } ?>

		<?php if($this->get_option('displayAddAllToCart')) { ?>
			<div class="woocommerce-shop-look-add-all-to-cart-container">

				<div class="woocommerce-shop-look-add-all-to-cart-count-container">
					<span class="woocommerce-shop-look-add-all-to-cart-count">0</span> <?php echo $textsProductsChoosen ?>
				</div>
				<div class="woocommerce-shop-look-add-all-to-cart-total-container">
					<?php echo $currencyPosition == "left" || $currencyPosition == "left_space" ?  get_woocommerce_currency_symbol() : '' ?> <span class="woocommerce-shop-look-add-all-to-cart-total">0</span><?php echo $currencyPosition == "right" || $currencyPosition == "right_space" ?  get_woocommerce_currency_symbol() : ''?>
				</div>
				<div class="woocommerce-shop-look-clear"></div>

				<form action="" method="POST" class="woocommerce-shop-look-add-all-to-cart">
					<input type="hidden" name="action" value="woocommerce_shop_look_add_all_to_cart">

					<button class="btn button btn-primary woocommerce-shop-look-add-all-to-cart-button" disabled="disabled"><?php echo $textsAddAllToCart ?></button>
				</form>
			</div>
		<?php } ?>

		<?php

		echo '</div>';
	}

	public function shortcode($atts, $content)
    {
    	global $product;

    	$original_product = $product;

        extract(shortcode_atts(array(
            'product_id' => '',
        ), $atts));

        ob_start();

        if(empty($product_id) && empty($product)) {
    		return esc_html__('No Product found', 'woocommerce-shop-look');
        } elseif(!empty($product_id)) {
        	$product = wc_get_product($product_id);
        }
        
        if(!$product) {
        	return;
        }

        $this->display_shop_look($product);

        $product = $original_product;

		$html = ob_get_contents();
		ob_end_clean();

		return $html;

	}

	public function add_to_cart()
	{
		ob_start();

		$cart_item_data = array();		

	    $product_id        = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $_POST['product_id'] ) );
	    $quantity          = empty( $_POST['quantity'] ) ? 1 : wc_stock_amount( $_POST['quantity'] );
	    $variation_id      = isset( $_POST['variation_id'] ) ? $_POST['variation_id'] : '';

	    $variation 		   = array();
	    foreach ($_POST as $postKey => $postValue) {
	    	if(in_array($postKey, array('product_id', 'quantity', 'variation_id', 'action', 'possible_variations') ) ) {
	    		continue;
	    	}
	    	$variation[$postKey] = $postValue;
	    }

	    $passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity, $variation_id, $variation, $cart_item_data );
		if ( $passed_validation) {
			$added = WC()->cart->add_to_cart( $product_id, $quantity, $variation_id, $variation );
			if(!$added) {
				echo html_entity_decode( json_encode(WC()->session->get( 'wc_notices'))) ;
				die();
			}
		}
		

        do_action( 'woocommerce_ajax_added_to_cart', $product_id );

        if($this->get_option('redirectShowMessage')) {
            wc_add_to_cart_message( $product_id );
        }

        // Return fragments
        WC_AJAX::get_refreshed_fragments();

	    die();
	}

	public function add_all_to_cart()
	{
		ob_start();

		$products = isset( $_POST['products'] ) ? $_POST['products'] : '';
		if(!empty($products) && is_array($products)) {
			foreach ($products as $product) {

		    	$quantity = 1;

			    $product_id        = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $product) );
			    $product = wc_get_product($product_id);

			    // Variable Product
			    if($product->is_type('variation')) {

			    	$variation_id = $product_id;
			    	$product_id = $product->get_parent_id();

					$passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity, $variation_id);
					if ( $passed_validation) {
						WC()->cart->add_to_cart( $product_id, $quantity, $variation_id );
					}
				// Simple Product
			    } else {
				    $passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );

					if ( $passed_validation) {
						WC()->cart->add_to_cart( $product_id, $quantity);
					}
			    }
			}
		} else {
			return false;
		}

        do_action( 'woocommerce_ajax_added_to_cart', $product_id );

        if($this->get_option('redirectShowMessage')) {
            wc_add_to_cart_message( $product_id );
        }

        // Return fragments
        WC_AJAX::get_refreshed_fragments();

	    die();
	}
}	