<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://www.welaunch.io
 * @since      1.0.0
 *
 * @package    WooCommerce_Shop_Look
 * @subpackage WooCommerce_Shop_Look/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    WooCommerce_Shop_Look
 * @subpackage WooCommerce_Shop_Look/includes
 * @author     Daniel Barenkamp <support@welaunch.io>
 */
class WooCommerce_Shop_Look_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		$loaded = load_plugin_textdomain(
			'woocommerce-shop-look',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
