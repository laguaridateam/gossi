<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.welaunch.io
 * @since      1.0.0
 *
 * @package    WooCommerce_Shop_Look
 * @subpackage WooCommerce_Shop_Look/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    WooCommerce_Shop_Look
 * @subpackage WooCommerce_Shop_Look/includes
 * @author     Daniel Barenkamp <support@welaunch.io>
 */
class WooCommerce_Shop_Look_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
