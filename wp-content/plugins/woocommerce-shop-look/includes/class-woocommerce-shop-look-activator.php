<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.welaunch.io
 * @since      1.0.0
 *
 * @package    WooCommerce_Shop_Look
 * @subpackage WooCommerce_Shop_Look/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    WooCommerce_Shop_Look
 * @subpackage WooCommerce_Shop_Look/includes
 * @author     Daniel Barenkamp <support@welaunch.io>
 */
class WooCommerce_Shop_Look_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
