<?php

/**
 * The plugin bootstrap file
 *
 *
 * @link              https://welaunch.io/plugins/woocommerce-shop-look/
 * @since             1.0.0
 * @package           WooCommerce_Shop_Look
 *
 * @wordpress-plugin
 * Plugin Name:       WooCommerce Shop the Look
 * Plugin URI:        https://welaunch.io/plugins/woocommerce-shop-look/
 * Description:       Show shop / get the look products (lookbooks) in your WooCommerce Shop
 * Version:           1.0.8
 * Author:            weLaunch
 * Author URI:        https://welaunch.io
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       woocommerce-shop-look
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-woocommerce-shop-look-activator.php
 */
function activate_WooCommerce_Shop_Look() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-woocommerce-shop-look-activator.php';
	WooCommerce_Shop_Look_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-woocommerce-shop-look-deactivator.php
 */
function deactivate_WooCommerce_Shop_Look() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-woocommerce-shop-look-deactivator.php';
	WooCommerce_Shop_Look_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_WooCommerce_Shop_Look' );
register_deactivation_hook( __FILE__, 'deactivate_WooCommerce_Shop_Look' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-woocommerce-shop-look.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_WooCommerce_Shop_Look() {

	$plugin_data = get_plugin_data( __FILE__ );
	$version = $plugin_data['Version'];

	$plugin = new WooCommerce_Shop_Look($version);
	$plugin->run();

	return $plugin;
}

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if ( is_plugin_active( 'woocommerce/woocommerce.php') && (is_plugin_active('redux-dev-master/redux-framework.php') || is_plugin_active('redux-framework/redux-framework.php') ||  is_plugin_active('welaunch-framework/welaunch-framework.php') ) ){
	$WooCommerce_Shop_Look = run_WooCommerce_Shop_Look();
} else {
	add_action( 'admin_notices', 'weLaunch_woocommerce_shop_look_installed_notice' );
}

function weLaunch_woocommerce_shop_look_installed_notice()
{
	?>
    <div class="error">
      <p><?php _e( 'WooCommerce Shop The Look requires the WooCommerce & weLaunch Framework plugin. Please install or activate them: https://www.welaunch.io/updates/welaunch-framework.zip', 'woocommerce-shop-look'); ?></p>
    </div>
    <?php
}