# Changelog
======
1.0.8
======
- NEW:	Added flatsome custom hooks
- FIX:	Wholesale plugin error
- FIX:	Removed stock status when no variation found

======
1.0.7
======
- FIX:	Price not shown on shortcode variation select
- FIX:	Price missing thousands seperator
- FIX:	Total price not rounded
- FIX:	Price display wrong when excl. TAX was set
- FIX:	Not matching variations will disable product button

======
1.0.6
======
- NEW:	Performance improvement
- FIX:	URL encoding issue for Greek Attribute Names
- FIX:	Currency symbol not showed when using currency position WITH space
- FIX:	WPML Keys missing

======
1.0.5
======
- FIX:	Deleting all shop look products not possible
- FIX:	Shortcode for elementor more dynamic (no need to enter product id)

======
1.0.4
======
- FIX:	Error not found in JS

======
1.0.3
======
- NEW:	Added support for "any" variations
- FIX:	fatal error on product pages

======
1.0.2
======
- NEW:	Performance option in general settings
- NEW:	Added support for porto & woodmart
- NEW:	Added is_purchaseable() function on add to cart & checkbox button
- NEW:	Alert issue when ajax failed
- FIX:	Using document jQuery listeners now

======
1.0.1
======
- FIX:	Moved the "Shop the Look proudcts" to linked products 
		section in backend in order to support variable products
		https://imgur.com/a/OsuJuW7

======
1.0.0
======
- Inital release