<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.welaunch.io
 * @since      1.0.0
 *
 * @package    WooCommerce_Shop_Look
 * @subpackage WooCommerce_Shop_Look/admin
 * @author     Daniel Barenkamp <support@welaunch.io>
 */

class WooCommerce_Shop_Look_Admin extends WooCommerce_Shop_Look {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	protected $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	protected $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->notice = "";

	}

    /**
     * Load Extensions
     * @author Daniel Barenkamp
     * @version 1.0.0
     * @since   1.0.0
     * @link    http://woocommerce.welaunch.io
     * @return  boolean
     */
    public function load_extensions()
    {
        // Load the theme/plugin options
        if (file_exists(plugin_dir_path(dirname(__FILE__)).'admin/options-init.php')) {
            require_once plugin_dir_path(dirname(__FILE__)).'admin/options-init.php';
        }
    }

	/**
	 * Init admin
	 *
	 * @since    1.0.0
	 */
    public function init()
    {
    	global $woocommerce, $woocommerce_shop_look_options;
        $this->options = $woocommerce_shop_look_options;

		if (!$this->get_option('enable')) {
			return false;
		}

		add_action('woocommerce_product_options_related', array($this, 'custom_fields') );
		add_action('woocommerce_process_product_meta', array($this, 'save_custom_fields') );
	}

	public function custom_fields()
	{
		global $post;

	    echo '<div class="product_custom_field">';
	    ?>

			<p class="form-field">
				<label for="woocommerce_shop_look_products"><?php esc_html_e( 'Shop the Look Products', 'woocommerce-dark-pattern' ); ?></label>
				<select class="wc-product-search" multiple="" style="width: 50%;" id="woocommerce_shop_look_products" name="woocommerce_shop_look_products[]" data-placeholder="Search for a product…" data-action="woocommerce_json_search_products_and_variations" data-exclude="<?php echo intval( $post->ID ); ?>" tabindex="-1" aria-hidden="true">
				<?php
					$product_ids = array_map( 'absint', (array) get_post_meta( $post->ID, 'woocommerce_shop_look_products', true ) );

					foreach ( $product_ids as $product_id ) {
						$product = wc_get_product( $product_id );
						if ( is_object( $product ) ) {
							echo '<option value="' . $product_id . '" selected="selected">' . wp_kses_post( html_entity_decode( $product->get_formatted_name(), ENT_QUOTES, get_bloginfo( 'charset' ) ) ) . '</option>';
						}
					}
				?>
				</select>
			</p>

		<?php

	     echo '</div>';

	}

	public function save_custom_fields($productId)
	{
		$product = wc_get_product($productId);
		
		if(isset($_POST['woocommerce_shop_look_products']) && !empty($_POST['woocommerce_shop_look_products'])) {

			$shopLookProducts    = isset( $_POST['woocommerce_shop_look_products'] ) ? array_filter( array_map( 'intval', $_POST['woocommerce_shop_look_products'] ) ) : array();
			$product->update_meta_data('woocommerce_shop_look_products', $shopLookProducts);
		}  else {
			$product->delete_meta_data('woocommerce_shop_look_products');
		}

		$product->save();
	}


}