<?php

    if ( ! class_exists( 'weLaunch' ) && ! class_exists( 'Redux' ) ) {
        return;
    }

    if( class_exists( 'weLaunch' ) ) {
        $framework = new weLaunch();
    } else {
        $framework = new Redux();
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "woocommerce_shop_look_options";

    $args = array(
        'opt_name' => 'woocommerce_shop_look_options',
        'use_cdn' => TRUE,
        'dev_mode' => FALSE,
        'display_name' => esc_html__('WooCommerce Shop the Look', 'woocommerce-shop-look'),
        'display_version' => '1.0.8',
        'page_title' => esc_html__('WooCommerce Shop the Look', 'woocommerce-shop-look'),
        'update_notice' => TRUE,
        'intro_text' => '',
        'footer_text' => '&copy; ' . date('Y') . ' weLaunch',
        'admin_bar' => false,
        'menu_type' => 'submenu',
        'menu_title' => esc_html__('Shop the Look', 'woocommerce-shop-look'),
        'allow_sub_menu' => TRUE,
        'page_parent' => 'woocommerce',
        'customizer' => FALSE,
        'default_mark' => '*',
        'hints' => array(
            'icon_position' => 'right',
            'icon_color' => 'lightgray',
            'icon_size' => 'normal',
            'tip_style' => array(
                'color' => 'light',
            ),
            'tip_position' => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect' => array(
                'show' => array(
                    'duration' => '500',
                    'event' => 'mouseover',
                ),
                'hide' => array(
                    'duration' => '500',
                    'event' => 'mouseleave unfocus',
                ),
            ),
        ),
        'output' => TRUE,
        'output_tag' => TRUE,
        'settings_api' => TRUE,
        'cdn_check_time' => '1440',
        'compiler' => TRUE,
        'page_permissions' => 'manage_options',
        'save_defaults' => TRUE,
        'show_import_export' => TRUE,
        'database' => 'options',
        'transient_time' => '3600',
        'network_sites' => TRUE,
    );

    global $weLaunchLicenses;
    if( (isset($weLaunchLicenses['woocommerce-shop-look']) && !empty($weLaunchLicenses['woocommerce-shop-look'])) || (isset($weLaunchLicenses['woocommerce-plugin-bundle']) && !empty($weLaunchLicenses['woocommerce-plugin-bundle'])) ) {
        $args['display_name'] = '<span class="dashicons dashicons-yes-alt" style="color: #9CCC65 !important;"></span> ' . $args['display_name'];
    } else {
        $args['display_name'] = '<span class="dashicons dashicons-dismiss" style="color: #EF5350 !important;"></span> ' . $args['display_name'];
    }

    $framework::setArgs( $opt_name, $args );


    $framework::setSection( $opt_name, array(
        'title'  => esc_html__('Shop the Look', 'woocommerce-shop-look' ),
        'id'     => 'general',
        'desc'   => esc_html__('Need support? Please use the comment function on codecanyon.', 'woocommerce-shop-look' ),
        'icon'   => 'el el-home',
    ) );

    $framework::setSection( $opt_name, array(
        'title'      => esc_html__('General', 'woocommerce-shop-look' ),
        'desc'       => __( 'To get auto updates please <a href="' . admin_url('tools.php?page=welaunch-framework') . '">register your License here</a>.', 'woocommerce-shop-look' ),
        'id'         => 'general-settings',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'enable',
                'type'     => 'checkbox',
                'title'    => esc_html__('Enable', 'woocommerce-shop-look' ),
                'subtitle' => esc_html__('Enable Shop the Look.', 'woocommerce-shop-look' ),
                'default'  => '1',
            ),
            array(
                'id'       => 'removeDefaultAddToCart',
                'type'     => 'checkbox',
                'title'    => esc_html__('Remove add to cart', 'woocommerce-shop-look' ),
                'subtitle'    => esc_html__('Remove add to cart of Shop the Look Products', 'woocommerce-shop-look'), 
                'default'  => '1',
            ),
            array(
                'id'       => 'performanceOnlyWooPages',
                'type'     => 'checkbox',
                'title'    => __('Performance: Scripts & Stylings', 'woocommerce-shop-look' ),
                'subtitle' => __('Only execute CSS & JS Files on product pages. Disable when you use shortcodes.', 'woocommerce-shop-look' ),
                'default'  => '1',
                'required' => array('enable','equals','1'),
            ),
        )
    ) );

    $framework::setSection( $opt_name, array(
        'title'      => esc_html__('Display', 'woocommerce-shop-look' ),
        'id'         => 'display',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'displayAddToCart',
                'type'     => 'checkbox',
                'title'    => esc_html__('Show a Add to Cart button', 'woocommerce-shop-look' ),
                'subtitle' => esc_html__('Display an Add to Cart Button', 'woocommerce-shop-look' ),
                'default'  => '1',
            ),
            array(
                'id'       => 'displayImage',
                'type'     => 'checkbox',
                'title'    => esc_html__( 'Display Image', 'woocommerce-shop-look' ),
                'default'  => '1',
            ),
            array(
                'id'       => 'displayTitle',
                'type'     => 'checkbox',
                'title'    => esc_html__( 'Display Title', 'woocommerce-shop-look' ),
                'default'  => '1',
            ),
            array(
                'id'       => 'displayCategories',
                'type'     => 'checkbox',
                'title'    => esc_html__( 'Display Categories', 'woocommerce-shop-look' ),
                'default'  => '1',
            ),
            array(
                'id'       => 'displayShortDescription',
                'type'     => 'checkbox',
                'title'    => esc_html__( 'Display ShortDescription', 'woocommerce-shop-look' ),
                'default'  => '1',
            ),
            array(
                'id'       => 'displayPrice',
                'type'     => 'checkbox',
                'title'    => esc_html__( 'Display Price', 'woocommerce-shop-look' ),
                'default'  => '1',
            ),
            array(
                'id'       => 'displayStock',
                'type'     => 'checkbox',
                'title'    => esc_html__( 'Display Stock', 'woocommerce-shop-look' ),
                'default'  => '1',
            ),
            array(
                'id'       => 'displaySKU',
                'type'     => 'checkbox',
                'title'    => esc_html__( 'Display SKU', 'woocommerce-shop-look' ),
                'default'  => '0',
            ),

            array(
                'id'       => 'displayAddAllToCart',
                'type'     => 'checkbox',
                'title'    => esc_html__( 'Display Add All to Cart', 'woocommerce-shop-look' ),
                'subtitle'    => esc_html__( 'Show checkboxes and an add all to cart button at the end of the listing.', 'woocommerce-shop-look' ),
                'default'  => '1',
            ),
            array(
                'id'       => 'displayPosition',
                'type'     => 'select',
                'title'    => esc_html__('Products Position', 'woocommerce-shop-look'),
                'subtitle' => esc_html__('Specify the positon of the products.', 'woocommerce-shop-look'),
                'default'  => 'woocommerce_product_meta_start',
                'options'  => array( 
                    'wp_footer' => esc_html__('Footer (Modal)', 'woocommerce-shop-look'),
                    'woocommerce_product_thumbnails' => esc_html__('Icon in Image', 'woocommerce-shop-look'),
                    'woocommerce_before_single_product' => esc_html__('Before Single Product', 'woocommerce-shop-look'),
                    'woocommerce_before_single_product_summary' => esc_html__('Before Single Product Summary', 'woocommerce-shop-look'),
                    'woocommerce_single_product_summary' => esc_html__('In Single Product Summary', 'woocommerce-shop-look'),
                    'woocommerce_product_meta_start' => esc_html__('Before Meta Information', 'woocommerce-shop-look'),
                    'woocommerce_product_meta_end' => esc_html__('After Meta Information', 'woocommerce-shop-look'),
                    'woocommerce_after_single_product_summary' => esc_html__('After Single Product Summary', 'woocommerce-shop-look'),
                    'woocommerce_after_single_product' => esc_html__('After Single Product', 'woocommerce-shop-look'),
                    'woocommerce_after_main_content' => esc_html__('After Main Product', 'woocommerce-shop-look'),
                    'flatsome_custom_single_product_1' => 'flatsome_custom_single_product_1',
                    'flatsome_custom_single_product_2' => 'flatsome_custom_single_product_2',
                    'flatsome_custom_single_product_3' => 'flatsome_custom_single_product_3',
                ),
            ),
            array(
                'id'       => 'displayPriority',
                'type'     => 'spinner',
                'title'    => esc_html__( 'Hook Priority', 'woocommerce-shop-look' ),
                'min'      => '1',
                'step'     => '1',
                'max'      => '999',
                'default'  => '5',
            ),
        )
    ) );

    $framework::setSection( $opt_name, array(
        'title'      => esc_html__('Styling', 'woocommerce-shop-look' ),
        'id'         => 'styling',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'        => 'textColor',
                'type'      => 'color',
                'title'    => esc_html__('Text Color', 'woocommerce-shop-look'), 
                'default'   => '#333333',
            ),
            array(
                'id'        => 'buttonBackgroundColor',
                'type'      => 'color',
                'title'    => esc_html__('Button Background Color', 'woocommerce-shop-look'), 
                'default'   => '#000000',
            ),
            array(
                'id'        => 'buttonTextColor',
                'type'      => 'color',
                'title'    => esc_html__('Button Text Color', 'woocommerce-shop-look'), 
                'default'   => '#ffffff',
            ),

      )
    ) );

    $framework::setSection( $opt_name, array(
        'title'      => esc_html__('Texts', 'woocommerce-shop-look' ),
        'id'         => 'texts',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'textsAddToCart',
                'type'     => 'text',
                'title'    => esc_html__( 'Add to Cart', 'woocommerce-shop-look' ),
                'default'  => esc_html__( 'Add to Cart', 'woocommerce-shop-look' ),
            ),
            array(
                'id'       => 'textsAddAllToCart',
                'type'     => 'text',
                'title'    => esc_html__( 'Add All to Cart', 'woocommerce-shop-look' ),
                'default'  => esc_html__( 'Add All to Cart', 'woocommerce-shop-look' ),
            ),
            array(
                'id'       => 'textsProductsChoosen',
                'type'     => 'text',
                'title'    => esc_html__( 'Products Choosen', 'woocommerce-shop-look' ),
                'default'  => esc_html__( 'Products Choosen', 'woocommerce-shop-look' ),
            ),
      )
    ) );